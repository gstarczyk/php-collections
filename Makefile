.PHONY: help
help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

pull: ## pull all images
	docker-compose pull

build: ## build images for all services
	COMPOSE_DOCKER_CLI_BUILD=0 docker-compose build

up: ## ap containers for all services
	docker-compose up -d --remove-orphans

down: ## stop and remove all services
	docker-compose down

stop: ## stop all services
	docker-compose stop

sh: ## run shell in php container
	docker-compose run -it php sh

install:
	docker-compose run php sh -c 'composer install'

qa: ## run tests
	docker-compose run php sh -c 'composer qa'

sa:
	docker-compose run php sh -c 'composer sa'

cs-fix:
	docker-compose run php sh -c 'composer cs-fix'

run: up install
