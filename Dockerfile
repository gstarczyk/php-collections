FROM php:8.2-cli

RUN apt-get update && apt-get install -y unzip \
    && pecl install xdebug \
	&& docker-php-ext-enable xdebug

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

WORKDIR /usr/src/php-collections
