<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;

/**
 * @template TValue of object
 * @extends AbstractSet<TValue>
 */
class ObjectsSet extends AbstractSet
{
    private string $elementClass;

    /**
     * @param class-string<TValue> $elementClass
     * @param Comparator<TValue>|null $comparator
     */
    public function __construct(string $elementClass, Comparator $comparator = null)
    {
        parent::__construct($comparator);
        $this->elementClass = $elementClass;
    }

    /**
     * @inheritdoc
     */
    protected function validateElement(mixed $element): void
    {
        if ($element === null) {
            throw new NullElementException('ObjectsSet does not accept null elements.');
        }
        if (!($element instanceof $this->elementClass)) {
            throw InvalidElementException::invalidType($this->elementClass, $element);
        }
    }
}
