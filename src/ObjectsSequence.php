<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;

/**
 * Not nullable sequence of objects that are instance of given class
 * @template TValue of object
 * @extends AbstractSequence<TValue>
 */
class ObjectsSequence extends AbstractSequence
{
    private string $elementClassName;

    /**
     * @param class-string<TValue> $elementClassName
     */
    public function __construct(string $elementClassName)
    {
        $this->elementClassName = $elementClassName;
    }

    /**
     * @inheritdoc
     */
    protected function validateElement(mixed $element): void
    {
        if (null === $element) {
            throw new NullElementException('Element cannot be NULL');
        }
        if (!($element instanceof $this->elementClassName)) {
            throw InvalidElementException::invalidType($this->elementClassName, $element);
        }
    }
}
