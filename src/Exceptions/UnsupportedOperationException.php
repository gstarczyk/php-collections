<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\Exceptions;

use RuntimeException;

class UnsupportedOperationException extends RuntimeException
{
}
