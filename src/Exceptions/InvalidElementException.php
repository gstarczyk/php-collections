<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\Exceptions;

use InvalidArgumentException;

/**
 * Thrown when given element is not valid in context of selected Collection
 */
class InvalidElementException extends InvalidArgumentException
{
    public const MESSAGE_INVALID_TYPE = 'Collection accept only elements with type of %s, but "%s" was given.';

    /**
     * @param string $expectedElementType
     * @param mixed $givenElement
     *
     * @return InvalidElementException
     */
    public static function invalidType($expectedElementType, $givenElement)
    {
        $givenElementType = (is_object($givenElement)) ? get_class($givenElement) : gettype($givenElement);
        $message = sprintf(self::MESSAGE_INVALID_TYPE, $expectedElementType, $givenElementType);

        return new self($message);
    }
}
