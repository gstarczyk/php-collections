<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\Exceptions;

class IndexOutOfBoundsException extends \OutOfBoundsException
{
}
