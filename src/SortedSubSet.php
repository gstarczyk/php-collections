<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use ArrayIterator;
use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NoSuchElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;
use Traversable;

/**
 * @template TValue
 * @implements SortedSet<TValue>
 */
class SortedSubSet implements SortedSet
{
    /**
     * @var SortedSet<TValue>
     */
    private SortedSet $parentSet;

    /**
     * @var TValue
     */
    private mixed $fromElement;

    /**
     * @var TValue
     */
    private mixed $toElement;

    /**
     * @param SortedSet<TValue> $parentSet
     */
    public function __construct(SortedSet $parentSet, mixed $fromElement, mixed $toElement)
    {
        $this->parentSet = $parentSet;
        $this->fromElement = $fromElement;
        $this->toElement = $toElement;
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->toArray());
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        return empty($this->toArray());
    }

    /**
     * @inheritDoc
     */
    public function contains(mixed $element): bool
    {
        if ($this->withinBoundaries($element)) {
            return $this->parentSet->contains($element);
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->toArray());
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        $all = $this->parentSet->toArray();
        $elements = array_filter($all, [$this, 'withinBoundaries']);

        return array_values($elements);
    }

    /**
     * @inheritDoc
     */
    public function add(mixed $element): bool
    {
        $this->validateElement($element);

        return $this->parentSet->add($element);
    }

    /**
     * @inheritDoc
     */
    public function remove(mixed $element): bool
    {
        if ($this->withinBoundaries($element)) {
            return $this->parentSet->remove($element);
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function containsAll(iterable $elements): bool
    {
        foreach ($elements as $element) {
            if (!$this->contains($element)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function addAll(iterable $elements): bool
    {
        $changed = false;
        foreach ($elements as $element) {
            if ($this->add($element)) {
                $changed = true;
            }
        }

        return $changed;
    }

    /**
     * @inheritDoc
     */
    public function retainAll(iterable $elements): bool
    {
        $changed = false;
        $collection = new MixedSequence($elements);
        foreach ($this->toArray() as $element) {
            if (!$collection->contains($element)) {
                $this->parentSet->remove($element);
                $changed = true;
            }
        }

        return $changed;
    }

    /**
     * @inheritDoc
     */
    public function removeAll(iterable $elements): bool
    {
        $changed = false;
        foreach ($elements as $element) {
            $removed = $this->remove($element);
            if ($removed) {
                $changed = true;
            }
        }

        return $changed;
    }

    /**
     * @inheritDoc
     */
    public function clear(): void
    {
        foreach ($this->toArray() as $element) {
            $this->remove($element);
        }
    }

    /**
     * @inheritDoc
     */
    public function equals(Set $set): bool
    {
        return ($set->count() == $this->count() && $this->containsAll($set));
    }

    /**
     * @return Comparator<TValue>
     */
    public function comparator(): Comparator
    {
        return $this->parentSet->comparator();
    }

    /**
     * @inheritDoc
     */
    public function subSet(mixed $fromElement, mixed $toElement): SortedSet
    {
        $this->validateElement($fromElement);
        $this->validateElement($toElement);

        return new SortedSubSet($this->parentSet, $fromElement, $toElement);
    }

    /**
     * @inheritDoc
     */
    public function headSet(mixed $toElement): SortedSet
    {
        $this->validateElement($toElement);

        return new SortedSubSet($this->parentSet, null, $toElement);
    }

    /**
     * @inheritDoc
     */
    public function tailSet(mixed $fromElement): SortedSet
    {
        $this->validateElement($fromElement);

        return new SortedSubSet($this->parentSet, $fromElement, null);
    }

    /**
     * @inheritDoc
     */
    public function first(): mixed
    {
        $this->assertNotEmpty('Cannot return first element from empty set.');
        $elements = $this->toArray();

        return reset($elements);
    }

    /**
     * @inheritDoc
     */
    public function last(): mixed
    {
        $this->assertNotEmpty('Cannot return last element from empty set.');
        $elements = $this->toArray();

        return end($elements);
    }

    /**
     * @param mixed $element
     * @return bool
     */
    private function withinBoundaries(mixed $element): bool
    {
        if ($this->fromElement === null) {
            $greaterOrEqual = true;
        } else {
            $greaterOrEqual = $this->comparator()->__invoke($element, $this->fromElement) >= 0;
        }
        if ($this->toElement === null) {
            $less = true;
        } else {
            $less = $this->comparator()->__invoke($element, $this->toElement) < 0;
        }

        return $greaterOrEqual && $less;
    }

    /**
     * @param mixed $element
     * @throws InvalidElementException
     */
    private function validateElement(mixed $element): void
    {
        if ($element === null) {
            throw new NullElementException('SortedSet do not accept NULL elements.');
        }
        if (!$this->withinBoundaries($element)) {
            throw new InvalidElementException('Given element is outside of Subset boundaries!');
        }
    }

    /**
     * @param string $message
     * @throws NoSuchElementException
     * @phpstan-assert non-empty-array $this->toArray()
     */
    private function assertNotEmpty(string $message): void
    {
        if ($this->isEmpty()) {
            throw new NoSuchElementException($message);
        }
    }
}
