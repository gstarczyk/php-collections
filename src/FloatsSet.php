<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;

/**
 * @extends AbstractSet<float>
 */
class FloatsSet extends AbstractSet
{
    /**
     * @inheritdoc
     */
    protected function validateElement(mixed $element): void
    {
        if ($element === null) {
            throw new NullElementException('FloatsSet does not accept null elements.');
        }
        if (!is_float($element)) {
            throw InvalidElementException::invalidType('float', $element);
        }
    }
}
