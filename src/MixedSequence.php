<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\NullElementException;

/**
 * Not nullable sequence of anything
 *
 * @extends AbstractSequence<mixed>
 * @implements Sequence<mixed>
 */
class MixedSequence extends AbstractSequence implements Sequence
{
    /**
     * @param iterable<mixed, mixed> $elements
     */
    public function __construct(iterable $elements = [])
    {
        $this->addAll($elements);
    }

    /**
     * @inheritdoc
     */
    protected function validateElement(mixed $element): void
    {
        if (null === $element) {
            throw new NullElementException('Element cannot be NULL');
        }
    }
}
