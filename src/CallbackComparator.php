<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Closure;

/**
 * @template T
 * @implements Comparator<T>
 */
class CallbackComparator implements Comparator
{
    /**
     * @var Closure(T, T): int
     */
    private Closure $callback;

    /**
     * Closure MUST return integer result
     * @param Closure(T, T): int $callback
     */
    public function __construct(Closure $callback)
    {
        $this->callback = $callback;
    }

    /**
     * @inheritdoc
     */
    public function __invoke(mixed $item1, mixed $item2): int
    {
        /** @phpstan-ignore-next-line */
        return $this->callback->__invoke($item1, $item2);
    }
}
