<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;

/**
 * @extends AbstractSortedSet<string>
 */
class StringsSortedSet extends AbstractSortedSet
{
    /**
     * @inheritDoc
     */
    protected function validateElement(mixed $element): void
    {
        if ($element === null) {
            throw new NullElementException('StringsSortedSet does not accept null elements.');
        }
        if (!is_string($element)) {
            throw InvalidElementException::invalidType('string', $element);
        }
    }

    /**
     * @return Comparator<string>
     */
    protected function createDefaultComparator(): Comparator
    {
        return new CallbackComparator(
            function ($element1, $element2) {
                return strcmp($element1, $element2);
            }
        );
    }
}
