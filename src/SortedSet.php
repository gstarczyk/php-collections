<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NoSuchElementException;

/**
 * A Set that further provides a total ordering on its elements.
 * The elements are ordered using their natural ordering, or by a Comparator
 * typically provided at sorted set creation time.
 *
 * The set's iterator will traverse the set in ascending element order.
 *
 * Several additional operations are provided to take advantage of the ordering.
 *
 * @template TValue
 * @extends Set<TValue>
 */
interface SortedSet extends Set
{
    /**
     * @return Comparator<TValue>
     */
    public function comparator(): Comparator;

    // Range-view

    /**
     * Returns a view of the portion of this set whose elements range from $fromElement (inclusive),
     * to $toElement (exclusive).
     * If $fromElement and $toElement are equal, the returned set is empty.
     *
     * Returned subSet is only VIEW, so any changes in parent set (within subset range) are visible in subset
     * and any changes in subset are visible in parent set.
     *
     * The returned set will throw an InvalidArgumentException on an attempt to insert an element outside its range.
     *
     * @param TValue $fromElement
     * @param TValue $toElement
     *
     * @return SortedSet<TValue>
     * @throws InvalidElementException - when $fromElement or $toElement are not valid elements for this set
     */
    public function subSet(mixed $fromElement, mixed $toElement): SortedSet;

    /**
     * Returns a view of the portion of this set whose elements are strictly less than $toElement.
     *
     * Returned subSet is only VIEW, so any changes in parent set (within subset range) are visible in subset
     * and any changes in subset are visible in parent set.
     *
     * The returned set will throw an InvalidArgumentException on an attempt to insert an element outside its range.
     *
     * @param TValue $toElement
     *
     * @return SortedSet<TValue>
     * @throws InvalidElementException - when $toElement is not valid element for this set
     */
    public function headSet(mixed $toElement): SortedSet;

    /**
     * Returns a view of the portion of this set whose elements are greater than or equal to $fromElement.
     *
     * Returned subSet is only VIEW, so any changes in parent set (within subset range) are visible in subset
     * and any changes in subset are visible in parent set.
     *
     * The returned set will throw an InvalidArgumentException on an attempt to insert an element outside its range.
     *
     * @param TValue $fromElement
     *
     * @return SortedSet<TValue>
     * @throws InvalidElementException - when $fromElement is not valid element for this set
     */
    public function tailSet(mixed $fromElement): SortedSet;

    // Endpoints

    /**
     * Returns the first (lowest) element currently in this set.
     *
     * @return TValue
     * @throws NoSuchElementException - if this set is empty
     */
    public function first(): mixed;

    /**
     * Returns the last (highest) element currently in this set.
     *
     * @return TValue
     * @throws NoSuchElementException - if this set is empty
     */
    public function last(): mixed;
}
