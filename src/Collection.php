<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Countable;
use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;
use Gstarczyk\PhpCollections\Exceptions\UnsupportedOperationException;
use IteratorAggregate;

/**
 * @template TValue
 * @extends IteratorAggregate<TValue>
 */
interface Collection extends IteratorAggregate, Countable
{
    /**
     * Ensures that this collection contains the specified element (optional operation).
     * Returns true if this collection changed as a result of the call. (Returns false if this collection
     * does not permit duplicates and already contains the specified element.)
     *
     * Collections that support this operation may place limitations on what elements may be added to this collection.
     * In particular, some collections will refuse to add null elements,
     * and others will impose restrictions on the type of elements that may be added.
     * Collection classes should clearly specify in their documentation any restrictions on what elements may be added.
     *
     * If a collection refuses to add a particular element for any reason other than that it already contains
     * the element, it must throw an exception (rather than returning false).
     * This preserves the invariant that a collection always contains the specified element after this call returns.
     *
     * @param TValue $element element whose presence in this collection is to be ensured
     * @return bool true if this collection changed as a result of the call
     * @throws UnsupportedOperationException - if the add operation is not supported by this collection
     * @throws InvalidElementException - if the class of the specified element prevents it
     *         from being added to this collection
     * @throws NullElementException - if the specified element is null and this collection does not permit null elements
     */
    public function add(mixed $element): bool;

    /**
     * Adds all the elements in the specified collection to this collection (optional operation).
     *
     * @param iterable<TValue> $elements elements to be added to this collection
     * @return bool true if this collection changed as a result of the call
     * @throws UnsupportedOperationException - if the addAll operation is not supported by this collection
     * @throws InvalidElementException - if the class of an element of the specified collection prevents it
     *         from being added to this collection
     * @throws NullElementException - if the specified collection contains a null element and this collection
     *         does not permit null elements, or if the specified collection is null
     */
    public function addAll(iterable $elements): bool;

    /**
     * Returns true if this collection contains the specified element.
     * More formally, returns true if and only if this list contains at least one element e
     * such that (o==null ? e==null : o.equals(e)).
     *
     * @param TValue $element element whose presence in this list is to be tested
     * @return bool true if this list contains the specified element
     * @throws InvalidElementException - if the type of the specified element is incompatible
     *         with this collection (optional)
     * @throws NullElementException - if the specified element is null and this collection does not
     *         permit null elements (optional)
     */
    public function contains(mixed $element): bool;

    /**
     * Returns true if this list contains all the elements of the specified collection.
     *
     * @param iterable<TValue> $elements elements to be checked for containment in this list
     * @return bool true if this collection contains all of the given elements
     * @throws InvalidElementException - if the types of one or more elements in the specified collection
     *         are incompatible with this collection (optional)
     * @throws NullElementException - if the specified collection contains one or more null elements
     *         and this collection does not permit null elements (optional), or if the specified collection is null.
     */
    public function containsAll(iterable $elements): bool;

    /**
     * Removes a single instance of the specified element from this collection, if it is present (optional operation).
     * More formally, removes an element e such that (o==null ? e==null : o.equals(e)),
     * if this collection contains one or more such elements.
     * Returns true if this collection contained the specified element
     * (or equivalently, if this collection changed as a result of the call).
     *
     * @param TValue $element
     * @return bool true if this collection changed as a result of the call
     * @throws InvalidElementException - if the type of the specified element
     *         is incompatible with this collection (optional)
     * @throws NullElementException - if the specified element is null and this collection
     *         does not permit null elements (optional)
     * @throws UnsupportedOperationException - if the remove operation is not supported by this collection
     */
    public function remove(mixed $element): bool;

    /**
     * Removes all of this collection's elements that are also contained in the specified collection.
     * After this call returns, this collection will contain no elements in common with the specified collection.
     *
     * @param iterable<TValue> $elements elements to be removed from this collection
     * @return bool true if this collection changed as a result of the call
     * @throws UnsupportedOperationException - if the removeAll method is not supported by this collection
     * @throws InvalidElementException - if the types of one or more elements in this collection are incompatible
     *         with the specified collection (optional)
     * @throws NullElementException - if this collection contains one or more null elements and the specified collection
     *         does not support null elements (optional), or if the specified collection is null
     */
    public function removeAll(iterable $elements): bool;

    /**
     * Returns true if this collection contains no elements.
     *
     * @return bool true if this collection contains no elements
     */
    public function isEmpty(): bool;

    /**
     * Removes all the elements from this collection.
     *
     * @return void
     * @throws UnsupportedOperationException - if the clear operation is not supported by this collection
     */
    public function clear(): void;

    /**
     * Returns the number of elements in this collection.
     *
     * @return int
     */
    public function count(): int;

    /**
     * Returns an array containing all the elements in this collection.
     * If this collection makes any guarantees as to what order its
     * elements are returned by its iterator, this method must return the
     * elements in the same order.
     *
     * The returned array will be "safe" in that no references to it
     * are maintained by this collection.
     * (In other words, this method must allocate a new array even if this
     * collection is backed by an array).
     * The caller is thus free to modify the returned array.
     *
     * This method acts as bridge between array-based and collection-based APIs.
     *
     * @return array<int, TValue>
     */
    public function toArray(): array;
}
