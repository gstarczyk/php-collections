<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\NoSuchElementException;

/**
 * @template TValue
 * @implements SortedSet<TValue>
 * @extends AbstractSet<TValue>
 */
abstract class AbstractSortedSet extends AbstractSet implements SortedSet
{
    /**
     * @param Comparator<TValue>|null $comparator optional
     */
    public function __construct(Comparator $comparator = null)
    {
        if ($comparator === null) {
            $comparator = $this->createDefaultComparator();
        }
        parent::__construct($comparator);
    }

    /**
     * @inheritDoc
     */
    public function add(mixed $element): bool
    {
        $changed = parent::add($element);
        if ($changed) {
            usort($this->elements, $this->comparator());
        }

        return $changed;
    }

    public function comparator(): Comparator
    {
        assert($this->comparator !== null);
        return $this->comparator;
    }

    /**
     * @inheritDoc
     */
    public function subSet(mixed $fromElement, mixed $toElement): SortedSet
    {
        $this->validateElement($fromElement);
        $this->validateElement($toElement);

        return new SortedSubSet($this, $fromElement, $toElement);
    }

    /**
     * @inheritDoc
     */
    public function headSet(mixed $toElement): SortedSet
    {
        $this->validateElement($toElement);

        return new SortedSubSet($this, null, $toElement);
    }

    /**
     * @inheritDoc
     */
    public function tailSet(mixed $fromElement): SortedSet
    {
        $this->validateElement($fromElement);

        return new SortedSubSet($this, $fromElement, null);
    }

    /**
     * @inheritDoc
     */
    public function first(): mixed
    {
        $this->assertNotEmpty('Cannot return first element from empty set.');

        return reset($this->elements);
    }

    /**
     * @inheritDoc
     */
    public function last(): mixed
    {
        $this->assertNotEmpty('Cannot return last element from empty set.');

        return end($this->elements);
    }

    /**
     * @return Comparator<TValue>
     */
    abstract protected function createDefaultComparator(): Comparator;

    /**
     * @param string $message
     * @throws NoSuchElementException
     * @phpstan-assert non-empty-array $this->elements
     */
    private function assertNotEmpty(string $message): void
    {
        if ($this->isEmpty()) {
            throw new NoSuchElementException($message);
        }
    }
}
