<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;

/**
 * @extends AbstractSet<string>
 */
class StringsSet extends AbstractSet
{
    /**
     * @inheritdoc
     */
    protected function validateElement(mixed $element): void
    {
        if ($element === null) {
            throw new NullElementException('StringsSet does not accept null elements.');
        }
        if (!is_string($element)) {
            throw InvalidElementException::invalidType('string', $element);
        }
    }
}
