<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;

/**
 * @extends AbstractSet<int>
 */
class IntegersSet extends AbstractSet
{
    /**
     * @inheritdoc
     */
    protected function validateElement(mixed $element): void
    {
        if ($element === null) {
            throw new NullElementException('IntegersSet does not accept null elements.');
        }
        if (!is_int($element)) {
            throw InvalidElementException::invalidType('integer', $element);
        }
    }
}
