<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Exception;
use Gstarczyk\PhpCollections\Exceptions\IndexOutOfBoundsException;
use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;
use Gstarczyk\PhpCollections\Exceptions\UnsupportedOperationException;

/**
 * An ordered collection. The user of this interface has precise control
 * over where in the sequence each element is inserted.
 * The user can access elements by their integer index (position in
 * the sequence), and search for elements in the sequence.
 *
 * Unlike sets, sequences typically allow duplicate elements,
 * and they typically allow multiple null elements if they allow
 * null elements at all (someone might wish to implement a sequence
 * that prohibits duplicates, by throwing runtime exceptions
 * when the user attempts to insert them).
 *
 * @template TValue
 * @extends Collection<TValue>
 */
interface Sequence extends Collection
{
    /**
     * Appends the specified element to the end of this sequence.
     *
     * @param TValue $element element to be appended to this sequence
     * @return bool as specified by {@link Collection#add}
     * @throws UnsupportedOperationException - if the add operation is not supported by this sequence
     * @throws InvalidElementException - if the class of the specified element prevents it
     *         from being added to this sequence
     * @throws NullElementException - if the specified element is null and this sequence does not permit null elements
     */
    public function add(mixed $element): bool;

    /**
     * Inserts the specified element at the specified position in this sequence (optional operation).
     * Shifts the element currently at that position (if any) and any subsequent elements to the right
     * (adds one to their indices).
     *
     * @param int $index index at which the specified element is to be inserted
     * @param TValue $element element to be inserted
     * @return void
     * @throws UnsupportedOperationException - if the add operation is not supported by this sequence
     * @throws InvalidElementException - if the class of the specified element prevents it
     *         from being added to this sequence
     * @throws NullElementException - if the specified element is null and this sequence does not permit null elements
     * @throws IndexOutOfBoundsException - if the index is out of range (index < 0 || index > size())
     */
    public function addAtIndex(int $index, mixed $element): void;

    /**
     * Appends all the elements in the specified collection to the end of
     * this sequence, in the order that they are returned by the specified
     * collection's iterator (optional operation). The behavior of this
     * operation is undefined if the specified collection is modified while
     * the operation is in progress. (Note that this will occur if the
     * specified collection is this sequence, and it's nonempty.)
     *
     * @param iterable<int, TValue> $elements elements to be added to this sequence
     * @return bool true if this sequence changed as a result of the call
     * @throws UnsupportedOperationException - if the addAll operation is not supported by this list
     * @throws InvalidElementException - if the class of an element of the specified collection prevents it
     *         from being added to this sequence
     * @throws NullElementException - if the specified collection contains one or more null elements
     *         and this sequence does not permit null elements, or if the specified collection is null
     * @see #add($element)
     */
    public function addAll(iterable $elements): bool;

    /**
     * Inserts all the elements in the specified collection into this
     * sequence at the specified position (optional operation). Shifts the
     * element currently at that position (if any) and any subsequent
     * elements to the right (increases their indices). The new elements
     * will appear in this sequence in the order that they are returned by the
     * specified collection's iterator.
     *
     * @param int $index index at which to insert the first element from the specified collection
     * @param iterable<int, TValue> $elements collection containing elements to be added to this sequence
     * @return bool true if this sequence changed as a result of the call
     * @throws UnsupportedOperationException - if the addAll operation is not supported by this sequence
     * @throws InvalidElementException - if the class of an element of the specified collection prevents it
     *         from being added to this sequence
     * @throws NullElementException - if the specified collection contains one or more null elements
     *         and this sequence does not permit null elements, or if the specified collection is null
     * @throws IndexOutOfBoundsException - if the index is out of range (index < 0 || index > size())
     */
    public function addAllAtIndex(int $index, iterable $elements): bool;

    /**
     * Removes all the elements from this sequence (optional operation).
     * The sequence will be empty after this call returns.
     *
     * @return void
     * @throws UnsupportedOperationException - if the clear operation is not supported by this sequence
     */
    public function clear(): void;

    /**
     * Returns true if this sequence contains the specified element.
     * More formally, returns true if and only if this sequence contains at least one element e
     * such that (o==null ? e==null : o.equals(e)).
     *
     * @param TValue $element element whose presence in this sequence is to be tested
     * @return bool true if this sequence contains the specified element
     * @throws InvalidElementException - if the type of the specified element is incompatible
     *         with this sequence (optional)
     * @throws NullElementException - if the specified element is null and this sequence
     *         does not permit null elements (optional)
     */
    public function contains(mixed $element): bool;

    /**
     * Returns true if this sequence contains all the elements of the specified collection.
     *
     * @param iterable<TValue> $elements collection to be checked for containment in this sequence
     * @return bool true if this sequence contains all the elements of the specified collection
     * @throws InvalidElementException - if the types of one or more elements in the specified collection are
     *         incompatible with this sequence (optional)
     * @throws NullElementException - if the specified collection contains one or more null elements and this sequence
     *         does not permit null elements (optional), or if the specified collection is null
     */
    public function containsAll(iterable $elements): bool;

    /**
     * Returns the element at the specified position in this sequence.
     *
     * @param int $index index of the element to return
     * @return TValue the element at the specified position in this sequence
     * @throws IndexOutOfBoundsException - if the index is out of range (index < 0 || index >= size())
     */
    public function get(int $index): mixed;

    /**
     * Returns the index of the first occurrence of the specified element in this sequence,
     * or -1 if this sequence does not contain the element.
     * More formally, returns the lowest index i such that (o==null ? get(i)==null : o.equals(get(i))),
     * or -1 if there is no such index.
     *
     * @param TValue $element element to search for
     * @return int the index of the first occurrence of the specified element in this sequence,
     *             or -1 if this sequence does not contain the element
     * @throws InvalidElementException - if the type of the specified element is incompatible
     *         with this sequence (optional)
     * @throws NullElementException if the specified element is null and this sequence
     *         does not permit null elements (optional)
     */
    public function indexOf(mixed $element): int;

    /**
     * Returns true if this sequence contains no elements.
     *
     * @return bool true if this sequence contains no elements
     */
    public function isEmpty(): bool;

    /**
     * Returns the index of the last occurrence of the specified element in this sequence,
     * or -1 if this sequence does not contain the element.
     * More formally, returns the highest index i such that (o==null ? get(i)==null : o.equals(get(i))),
     * or -1 if there is no such index.
     *
     * @param TValue $element element to search for
     * @return int the index of the first occurrence of the specified element in this sequence,
     *         or -1 if this sequence does not contain the element
     * @throws InvalidElementException - if the type of the specified element is incompatible
     *         with this sequence (optional)
     * @throws NullElementException if the specified element is null and this sequence
     *         does not permit null elements (optional)
     */
    public function lastIndexOf(mixed $element): int;

    /**
     * Removes the element at the specified position in this sequence (optional operation).
     * Shifts any subsequent elements to the left (subtracts one from their indices).
     * Returns the element that was removed from the sequence.
     *
     * @param int $index the index of the element to be removed
     * @return TValue the element previously at the specified position
     * @throws UnsupportedOperationException - if the remove operation is not supported by this sequence
     * @throws IndexOutOfBoundsException - if the index is out of range (index < 0 || index >= size())
     */
    public function removeIndex(int $index): mixed;

    /**
     * Removes the first occurrence of the specified element from this sequence, if it is present (optional operation).
     * If this sequence does not contain the element, it is unchanged. More formally, removes the element
     * with the lowest index i such that (o==null ? get(i)==null : o.equals(get(i))) (if such an element exists).
     * Returns true if this sequence contained the specified element
     * (or equivalently, if this sequence changed as a result of the call).
     *
     * @param TValue $element element to be removed from this sequence, if present
     * @return bool true if this sequence contained the specified element
     * @throws InvalidElementException - if the type of the specified element is incompatible
     *         with this sequence (optional)
     * @throws NullElementException - if the specified element is null and this sequence
     *         does not permit null elements (optional)
     * @throws UnsupportedOperationException - if the remove operation is not supported by this sequence
     */
    public function remove(mixed $element): bool;

    /**
     * Removes from this sequence all of its elements that are contained
     * in the specified collection (optional operation).
     *
     * @param iterable<TValue> $elements elements to be removed from this sequence
     * @return bool true if this sequence changed as a result of the call
     * @throws UnsupportedOperationException - if the removeAll operation is not supported by this sequence
     * @throws InvalidElementException - if the class of an element of this sequence is incompatible
     *         with the specified collection (optional)
     * @throws NullElementException - if this sequence contains a null element and the specified collection
     *         does not permit null elements (optional), or if the specified collection is null
     */
    public function removeAll(iterable $elements): bool;

    /**
     * Retains only the elements in this sequence that are contained in the specified collection (optional operation).
     * In other words, removes from this sequence all of its elements
     * that are not contained in the specified collection.
     *
     * @param iterable<TValue> $elements collection containing elements to be retained in this sequence
     * @return bool true if this sequence changed as a result of the call
     * @throws UnsupportedOperationException - if the retainAll operation is not supported by this sequence
     * @throws InvalidElementException - if the class of an element of this sequence is
     *         incompatible with the specified collection (optional)
     * @throws NullElementException - if this sequence contains a null element and the specified collection
     *         does not permit null elements (optional), or if the specified collection is null
     * @throws Exception when cannot retrieve an iterator
     */
    public function retainAll(iterable $elements): bool;

    /**
     * Replaces the element at the specified position in this sequence with the specified element (optional operation).
     *
     * @param int $index index of the element to replace
     * @param TValue $element element to be stored at the specified position
     * @return TValue the element previously at the specified position
     * @throws UnsupportedOperationException - if the set operation is not supported by this sequence
     * @throws NullElementException - if the specified element is null and this sequence does not permit null elements
     * @throws InvalidElementException if the type of the specified element is incompatible
     *         with this sequence (optional)
     * @throws IndexOutOfBoundsException - if the index is out of range (index < 0 || index >= size())
     */
    public function set(int $index, mixed $element): mixed;

    /**
     * Returns the number of elements in this sequence.
     *
     * @return int the number of elements in this sequence
     */
    public function count(): int;

    /**
     * Sorts this sequence according to the order induced by the specified Comparator.
     * All elements in this sequence must be mutually comparable using the specified comparator
     * (that is, c.compare(e1, e2) must not throw a ClassCastException for any elements e1 and e2 in the sequence).
     *
     * If the specified comparator is null then all elements in this sequence must implement the Comparable interface
     * and the elements' natural ordering should be used.
     *
     * @param Comparator<TValue> $comparator the Comparator used to compare sequence elements.
     * @return void
     * @throws InvalidElementException - if the sequence contains elements that are not mutually comparable
     *         using the specified comparator
     * @throws UnsupportedOperationException - if the sequence's sequence-iterator does not support the set operation
     */
    public function sort(Comparator $comparator): void;

    /**
     * Returns a view of the portion of this sequence between the specified fromIndex, inclusive,
     * and toIndex, exclusive. (If fromIndex and toIndex are equal, the returned sequence is empty.)
     * The returned sequence is backed by this sequence, so non-structural changes in the returned sequence
     * are reflected in this sequence, and vice-versa.
     * The returned sequence supports all of the optional sequence operations supported by this sequence.
     *
     * @param int $fromIndex low endpoint (inclusive) of the subSequence
     * @param int $toIndex high endpoint (exclusive) of the subSequence
     * @return Sequence<TValue> a view of the specified range within this sequence
     * @throws IndexOutOfBoundsException for an illegal endpoint index value
     *         (fromIndex < 0 || toIndex > size || fromIndex > toIndex)
     */
    public function subSequence(int $fromIndex, int $toIndex): Sequence;

    /**
     * Returns an array containing all the elements in this sequence in proper order (from first to last element)
     *
     * @return array<TValue>
     */
    public function toArray(): array;
}
