<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use ArrayIterator;
use Gstarczyk\PhpCollections\Exceptions\IndexOutOfBoundsException;
use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Traversable;

/**
 * @template TValue
 * @implements  Sequence<TValue>
 */
class SubSequence implements Sequence
{
    /**
     * @var Sequence<TValue>
     */
    private Sequence $parentSequence;
    private int $offset;
    private int $size;

    /**
     * @param Sequence<TValue> $parentSequence
     */
    public function __construct(Sequence $parentSequence, int $fromIndex, int $toIndex)
    {
        $this->parentSequence = $parentSequence;
        $this->offset = $fromIndex;
        $this->size = $toIndex - $fromIndex;
    }

    /**
     * @inheritdoc
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->toArray());
    }

    /**
     * @inheritdoc
     */
    public function add(mixed $element): bool
    {
        $this->parentSequence->addAtIndex($this->offset + $this->size, $element);
        ++$this->size;

        return true;
    }

    /**
     * @inheritdoc
     */
    public function addAtIndex(int $index, mixed $element): void
    {
        $this->validateIndex($index);
        $this->parentSequence->addAtIndex($this->offset + $index, $element);
        ++$this->size;
    }

    /**
     * @inheritdoc
     */
    public function addAll(iterable $elements): bool
    {
        $this->parentSequence->addAllAtIndex($this->offset + $this->size, $elements);
        $count = is_array($elements) ? count($elements) : iterator_count($elements);
        $this->size += $count;

        return $count > 0;
    }

    /**
     * @inheritdoc
     */
    public function addAllAtIndex(int $index, iterable $elements): bool
    {
        $this->validateIndex($index);
        $this->parentSequence->addAllAtIndex($this->offset + $index, $elements);
        $count = is_array($elements) ? count($elements) : iterator_count($elements);
        $this->size += $count;

        return $count > 0;
    }

    /**
     * @inheritdoc
     */
    public function clear(): void
    {
        for ($index = 0; $index < $this->size; ++$index) {
            $this->parentSequence->removeIndex($this->offset);
        }
        $this->size = 0;
    }

    /**
     * @inheritdoc
     */
    public function contains(mixed $element): bool
    {
        return ($this->indexOf($element) >= 0);
    }

    /**
     * @inheritdoc
     */
    public function containsAll(iterable $elements): bool
    {
        foreach ($elements as $element) {
            if (!$this->contains($element)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function get(int $index): mixed
    {
        $this->validateIndex($index);

        return $this->parentSequence->get($this->offset + $index);
    }

    /**
     * @inheritdoc
     */
    public function indexOf(mixed $element): int
    {
        foreach ($this->toArray() as $index => $elem) {
            if ($elem == $element) {
                return $index;
            }
        }

        return -1;
    }

    /**
     * @inheritdoc
     */
    public function isEmpty(): bool
    {
        return $this->count() == 0;
    }

    /**
     * @inheritdoc
     */
    public function lastIndexOf(mixed $element): int
    {
        $lastIndex = -1;
        foreach ($this->toArray() as $index => $elem) {
            if ($elem == $element) {
                $lastIndex = $index;
            }
        }

        return $lastIndex;
    }

    /**
     * @inheritdoc
     */
    public function removeIndex(int $index): mixed
    {
        $this->validateIndex($index);
        $element = $this->parentSequence->removeIndex($this->offset + $index);
        --$this->size;

        return $element;
    }

    /**
     * @inheritdoc
     */
    public function remove(mixed $element): bool
    {
        $changed = false;
        $index = $this->indexOf($element);
        if ($index > -1) {
            $this->removeIndex($index);
            $changed = true;
        }

        return $changed;
    }

    /**
     * @inheritdoc
     */
    public function removeAll(iterable $elements): bool
    {
        $changed = false;
        foreach ($elements as $element) {
            while ($this->remove($element)) {
                $changed = true;
            }
        }

        return $changed;
    }

    /**
     * @inheritdoc
     */
    public function retainAll(iterable $elements): bool
    {
        if ($elements instanceof Collection) {
            $collection = $elements;
        } else {
            $collection = new MixedSequence();
            $collection->addAll($elements);
        }
        $changed = false;
        foreach ($this->getIterator() as $element) {
            if (!$collection->contains($element)) {
                $changed = true;
                while ($this->remove($element)) {
                }
            }
        }

        return $changed;
    }

    /**
     * @inheritdoc
     */
    public function set(int $index, mixed $element): mixed
    {
        $this->validateIndex($index);
        return $this->parentSequence->set($this->offset + $index, $element);
    }

    /**
     * @inheritdoc
     */
    public function count(): int
    {
        return $this->size;
    }

    /**
     * @inheritdoc
     */
    public function sort(Comparator $comparator): void
    {
        $elements = $this->toArray();
        usort($elements, [$comparator, '__invoke']);
        foreach ($elements as $index => $element) {
            $this->set($index, $element);
        }
    }

    /**
     * @inheritdoc
     */
    public function subSequence(int $fromIndex, int $toIndex): Sequence
    {
        return new self($this->parentSequence, $this->offset + $fromIndex, $this->offset + $toIndex);
    }

    /**
     * @inheritdoc
     */
    public function toArray(): array
    {
        $parentElements = $this->parentSequence->toArray();

        return array_splice($parentElements, $this->offset, $this->size);
    }

    /**
     * @param int $index
     * @throws IndexOutOfBoundsException if the index is out of range
     * @throws InvalidElementException when given index is not integer
     */
    private function validateIndex(int $index): void
    {
        if ($index < 0 || $index >= $this->count()) {
            throw new IndexOutOfBoundsException(
                sprintf(
                    'Invalid sequence index. Allowed range (%u, %u), but %u was given.',
                    0,
                    $this->count(),
                    $index
                )
            );
        }
    }
}
