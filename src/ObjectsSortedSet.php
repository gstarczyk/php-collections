<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;
use RuntimeException;

/**
 * @template TValue of object
 * @extends AbstractSortedSet<TValue>
 */
class ObjectsSortedSet extends AbstractSortedSet
{
    private string $elementClass;

    /**
     * @param class-string<TValue> $elementClass
     * @param Comparator<TValue> $comparator
     */
    public function __construct(string $elementClass, Comparator $comparator)
    {
        parent::__construct($comparator);
        $this->elementClass = $elementClass;
    }

    /**
     * @inheritdoc
     */
    protected function validateElement(mixed $element): void
    {
        if ($element === null) {
            throw new NullElementException('ObjectsSortedSet does not accept null elements.');
        }
        if (!($element instanceof $this->elementClass)) {
            throw InvalidElementException::invalidType($this->elementClass, $element);
        }
    }

    /**
     * @return Comparator<TValue>
     */
    protected function createDefaultComparator(): Comparator
    {
        throw new RuntimeException('ObjectsSet do not provide default comparator');
    }
}
