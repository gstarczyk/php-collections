<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;

/**
 * Not nullable sequence of integers
 * @extends ScalarsSequence<int>
 * @implements Sequence<int>
 */
class IntegersSequence extends ScalarsSequence implements Sequence
{
    /**
     * @inheritdoc
     */
    protected function validateElement(mixed $element): void
    {
        if (null === $element) {
            throw new NullElementException('Element cannot be NULL');
        }
        if (!is_int($element)) {
            throw InvalidElementException::invalidType('integer', $element);
        }
    }
}
