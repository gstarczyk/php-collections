<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;

/**
 * Not nullable sequence of strings
 *
 * @extends ScalarsSequence<string>
 * @implements Sequence<string>
 */
class StringsSequence extends ScalarsSequence implements Sequence
{
    /**
     * @inheritdoc
     */
    protected function validateElement(mixed $element): void
    {
        if (null === $element) {
            throw new NullElementException('Element cannot be NULL');
        }
        if (!is_string($element)) {
            throw InvalidElementException::invalidType('string', $element);
        }
    }
}
