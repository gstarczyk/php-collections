<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;
use Gstarczyk\PhpCollections\Exceptions\UnsupportedOperationException;
use Iterator;
use Traversable;

/**
 * A collection that contains no duplicate elements.
 * More formally, sets contain no pair of elements e1 and e2 such that e1.equals(e2), and at most one null element.
 * As implied by its name, this interface models the mathematical set abstraction.
 *
 * @template TValue
 * @extends Collection<TValue>
 */
interface Set extends Collection
{
    /**
     * Returns the number of elements in this set (its cardinality).
     *
     * @return int the number of elements in this set (its cardinality)
     */
    public function count(): int;

    /**
     * Returns true if this set contains no elements.
     *
     * @return bool true if this set contains no elements
     */
    public function isEmpty(): bool;

    /**
     * Returns true if this set contains the specified element.
     * More formally, returns true if and only if this set
     * contains an element "e" such that
     * (o==null ? e==null : o.equals(e)).
     *
     * @param TValue $element element whose presence in this set is to be tested
     * @return bool true if this set contains the specified element
     * @throws InvalidElementException if the type of the specified element
     *         is incompatible with this set
     * @throws NullElementException if the specified element is null and this
     *         set does not permit null elements
     */
    public function contains(mixed $element): bool;

    /**
     * Returns an iterator over the elements in this set.  The elements are
     * returned in no particular order (unless this set is an instance of some
     * class that provides a guarantee).
     *
     * @return Iterator an iterator over the elements in this set
     */
    public function getIterator(): Traversable;

    /**
     * Returns an array containing all the elements in this set.
     * If this set makes any guarantees as to what order its elements
     * are returned by its iterator, this method must return the
     * elements in the same order.
     *
     * The returned array will be "safe" in that no references to it
     * are maintained by this set.  (In other words, this method must
     * allocate a new array even if this set is backed by an array).
     * The caller is thus free to modify the returned array.
     *
     * This method acts as bridge between array-based and collection-based
     * APIs.
     *
     * @return array<int, TValue> - array containing all the elements in this set
     */
    public function toArray(): array;

    // Modification Operations

    /**
     * Adds the specified element to this set if it is not already present
     * (optional operation).  More formally, adds the specified element
     * "e" to this set if the set contains no element "e2"
     * such that (e==null ? e2==null : e.equals(e2)).
     * If this set already contains the element, the call leaves the set
     * unchanged and returns false.  In combination with the
     * restriction on constructors, this ensures that sets never contain
     * duplicate elements.
     *
     * The stipulation above does not imply that sets must accept all
     * elements; sets may refuse to add any particular element, including
     * null, and throw an exception, as described in the
     * specification for {@see Collection.add}.
     * Individual set implementations should clearly document any
     * restrictions on the elements that they may contain.
     *
     * @param TValue $element element to be added to this set
     * @return bool true if this set did not already contain the specified
     *         element
     * @throws UnsupportedOperationException if the "add" operation
     *         is not supported by this set
     * @throws InvalidElementException if the class of the specified element
     *         prevents it from being added to this set
     * @throws NullElementException if the specified element is null and this
     *         set does not permit null elements
     */
    public function add(mixed $element): bool;

    /**
     * Removes the specified element from this set if it is present
     * (optional operation).  More formally, removes an element "e"
     * such that (o==null ? e==null : o.equals(e)), if
     * this set contains such an element.  Returns true if this set
     * contained the element (or equivalently, if this set changed as a
     * result of the call).  (This set will not contain the element once the
     * call returns.)
     *
     * @param TValue $element object to be removed from this set, if present
     * @return bool true if this set contained the specified element
     * @throws InvalidElementException if the type of the specified element
     *         is incompatible with this set
     * @throws NullElementException if the specified element is null and this
     *         set does not permit null elements
     * @throws UnsupportedOperationException if the remove operation
     *         is not supported by this set
     */
    public function remove(mixed $element): bool;

    // Bulk Operations

    /**
     * Returns true if this set contains all the elements of the
     * specified collection.  If the specified collection is also a set, this
     * method returns true if it is a subset of this set.
     *
     * @param iterable<TValue> $elements elements to be checked for containment in this set
     * @return bool true if this set contains all the elements of the
     *         specified collection
     * @throws InvalidElementException if the types of one or more elements
     *         in the specified collection are incompatible with this
     *         set
     * @throws NullElementException if the specified collection contains one
     *         or more null elements and this set does not permit null
     *         elements or if the specified collection is null
     */
    public function containsAll(iterable $elements): bool;

    /**
     * Adds all the elements in the specified collection to this set if
     * they're not already present (optional operation).  If the specified
     * collection is also a set, the <tt>addAll</tt> operation effectively
     * modifies this set so that its value is the <i>union</i> of the two
     * sets.  The behavior of this operation is undefined if the specified
     * collection is modified while the operation is in progress.
     *
     * @param iterable<TValue> $elements elements to be added to this set
     * @return bool true if this set changed as a result of the call
     *
     * @throws UnsupportedOperationException if the <tt>addAll</tt> operation
     *         is not supported by this set
     * @throws InvalidElementException if the class of an element of the
     *         specified collection prevents it from being added to this set
     * @throws NullElementException if the specified collection contains one
     *         or more null elements and this set does not permit null
     *         elements, or if the specified collection is null
     */
    public function addAll(iterable $elements): bool;

    /**
     * Retains only the elements in this set that are contained in the
     * specified collection (optional operation).  In other words, removes
     * from this set all of its elements that are not contained in the
     * specified collection.  If the specified collection is also a set, this
     * operation effectively modifies this set so that its value is the
     * intersection of the two sets.
     *
     * @param iterable<TValue> $elements elements to be retained in this set
     * @return bool true if this set changed as a result of the call
     * @throws UnsupportedOperationException if the retainAll operation
     *         is not supported by this set
     * @throws InvalidElementException if the class of an element of this set
     *         is incompatible with the specified collection
     * @throws NullElementException if this set contains a null element and the
     *         specified collection does not permit null elements
     *         or if the specified elements is null
     */
    public function retainAll(iterable $elements): bool;

    /**
     * Removes from this set all of its elements that are contained in the
     * specified collection (optional operation).  If the specified
     * collection is also a set, this operation effectively modifies this
     * set so that its value is the <i>asymmetric set difference</i> of
     * the two sets.
     *
     * @param iterable<TValue> $elements elements to be removed from this set
     * @return bool true if this set changed as a result of the call
     * @throws UnsupportedOperationException if the "removeAll" operation
     *         is not supported by this set
     * @throws InvalidElementException if the class of an element of this set
     *         is incompatible with the specified collection
     * @throws NullElementException if this set contains a null element and the
     *         specified collection does not permit null elements
     *         or if the specified collection is null
     */
    public function removeAll(iterable $elements): bool;

    /**
     * Removes all the elements from this set (optional operation).
     * The set will be empty after this call returns.
     *
     * @throws UnsupportedOperationException if the <tt>clear</tt> method
     *         is not supported by this set
     */
    public function clear(): void;

    // Comparison and hashing

    /**
     * Compares the specified object with this set for equality.
     * Returns true if the specified object is also a set, the two sets
     * have the same size, and every member of the specified set is
     * contained in this set (or equivalently, every member of this set is
     * contained in the specified set).  This definition ensures that the
     * equals method works properly across different implementations of the
     * set interface.
     *
     * @param Set<TValue> $set to be compared for equality with this set
     * @return bool true if the specified object is equal to this set
     */
    public function equals(Set $set): bool;
}
