<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use ArrayIterator;
use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;
use Traversable;

/**
 * @template TValue
 * @implements Collection<TValue>
 */
abstract class AbstractCollection implements Collection
{
    /**
     * @var array<TValue>
     */
    protected array $elements = [];

    /**
     * @inheritdoc
     */
    public function addAll(mixed $elements): bool
    {
        foreach ($elements as $element) {
            $this->add($element);
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function containsAll(iterable $elements): bool
    {
        foreach ($elements as $element) {
            if (!$this->contains($element)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function removeAll(iterable $elements): bool
    {
        $changed = false;
        foreach ($elements as $element) {
            while ($this->remove($element)) {
                $changed = true;
            }
        }

        return $changed;
    }

    /**
     * @inheritdoc
     */
    public function isEmpty(): bool
    {
        return empty($this->toArray());
    }

    /**
     * @inheritdoc
     */
    public function clear(): void
    {
        $this->elements = [];
    }

    /**
     * @inheritdoc
     */
    public function count(): int
    {
        return count($this->toArray());
    }

    /**
     * @inheritdoc
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->toArray());
    }

    /**
     * Check if given element meets the requirements
     * for example: element is instance of selected class
     *
     * @param mixed $element
     * @throws InvalidElementException if the type of the specified element is incompatible
     *         with this collection
     * @throws NullElementException - given element is null and the specified collection
     *         does not support null elements (optional)
     */
    abstract protected function validateElement(mixed $element): void;

    /**
     * @param iterable<TValue> $elements
     * @throws InvalidElementException if the type of any element from given elements
     *         is incompatible with this collection
     * @throws NullElementException - given elements contains one or more null elements and the specified collection
     *         does not support null elements (optional), or if the specified collection is null
     *
     */
    protected function validateElements(iterable $elements): void
    {
        foreach ($elements as $element) {
            $this->validateElement($element);
        }
    }
}
