<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

/**
 * @template T
 */
interface Comparator
{
    /**
     * Compare specified items.
     * Return 1 if item1 is greater than item2 (item1 > item2).
     * Return 0 if items are equal (item1 == item2).
     * Return -1 if item2 is greater than item1 (item2 > item1).
     *
     * @param T $item1
     * @param T $item2
     * @return int
     */
    public function __invoke(mixed $item1, mixed $item2): int;
}
