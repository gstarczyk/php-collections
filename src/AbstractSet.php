<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;
use Gstarczyk\PhpCollections\Exceptions\UnsupportedOperationException;

/**
 * @template TValue
 * @implements Set<TValue>
 * @extends AbstractCollection<TValue>
 */
abstract class AbstractSet extends AbstractCollection implements Set
{
    /**
     * Class using to compare elements in Set::contains() method
     * @var Comparator<TValue>|null
     */
    protected ?Comparator $comparator;

    /**
     * @param Comparator<TValue>|null $comparator
     */
    public function __construct(Comparator $comparator = null)
    {
        $this->comparator = $comparator;
    }

    /**
     * Ensures that this collection contains the specified element (optional operation).
     * Returns true if this collection changed as a result of the call. (Returns false if this collection
     * does not permit duplicates and already contains the specified element.)
     *
     * Collections that support this operation may place limitations on what elements may be added to this collection.
     * In particular, some collections will refuse to add null elements,
     * and others will impose restrictions on the type of elements that may be added.
     * Collection classes should clearly specify in their documentation any restrictions on what elements may be added.
     *
     * If a collection refuses to add a particular element for any reason other than that it already contains
     * the element, it must throw an exception (rather than returning false).
     * This preserves the invariant that a collection always contains the specified element after this call returns.
     *
     * @param TValue $element element whose presence in this collection is to be ensured
     * @return bool true if this collection changed as a result of the call
     * @throws UnsupportedOperationException - if the add operation is not supported by this collection
     * @throws InvalidElementException - if the class of the specified element prevents it
     *         from being added to this collection
     * @throws NullElementException - if the specified element is null and this collection does not permit null elements
     */
    public function add(mixed $element): bool
    {
        $changed = false;
        $this->validateElement($element);
        if (!$this->contains($element)) {
            $this->elements[] = $element;
            $changed = true;
        }

        return $changed;
    }

    /**
     * Returns true if this collection contains the specified element.
     * More formally, returns true if and only if this list contains at least one element e
     * such that (o==null ? e==null : o.equals(e)).
     *
     * @param TValue $element element whose presence in this list is to be tested
     * @return bool true if this list contains the specified element
     * @throws InvalidElementException - if the type of the specified element is incompatible
     *         with this collection (optional)
     * @throws NullElementException - if the specified element is null and this collection does not
     *         permit null elements (optional)
     */
    public function contains(mixed $element): bool
    {
        $comparator = $this->comparator;
        if ($comparator !== null) {
            $found = array_filter($this->elements, function (mixed $item) use ($element, $comparator) {
                return $comparator($item, $element) === 0;
            });

            return count($found) > 0;
        }

        return in_array($element, $this->elements);
    }

    /**
     * Removes a single instance of the specified element from this collection, if it is present (optional operation).
     * More formally, removes an element e such that (o==null ? e==null : o.equals(e)),
     * if this collection contains one or more such elements.
     * Returns true if this collection contained the specified element
     * (or equivalently, if this collection changed as a result of the call).
     *
     * @param TValue $element
     * @return bool true if this collection changed as a result of the call
     * @throws InvalidElementException - if the type of the specified element
     *         is incompatible with this collection (optional)
     * @throws NullElementException - if the specified element is null and this collection
     *         does not permit null elements (optional)
     * @throws UnsupportedOperationException - if the remove operation is not supported by this collection
     */
    public function remove(mixed $element): bool
    {
        $this->validateElement($element);

        $changed = false;
        foreach ($this->elements as $index => $elem) {
            if ($this->areEquals($elem, $element)) {
                $changed = true;
                unset($this->elements[$index]);
            }
        }

        return $changed;
    }

    /**
     * @param TValue $element1
     * @param TValue $element2
     * @return bool
     */
    private function areEquals(mixed $element1, mixed $element2): bool
    {
        if ($this->comparator !== null) {
            return $this->comparator->__invoke($element1, $element2) === 0;
        } else {
            return $element1 == $element2;
        }
    }

    /**
     * @inheritdoc
     */
    public function toArray(): array
    {
        return array_values($this->elements);
    }

    /**
     * Retains only the elements in this set that are contained in the
     * specified collection (optional operation).  In other words, removes
     * from this set all of its elements that are not contained in the
     * specified collection.  If the specified collection is also a set, this
     * operation effectively modifies this set so that its value is the
     * intersection of the two sets.
     *
     * @param iterable<TValue> $elements elements to be retained in this set
     * @return bool true if this set changed as a result of the call
     * @throws UnsupportedOperationException if the retainAll operation
     *         is not supported by this set
     * @throws InvalidElementException if the class of an element of this set
     *         is incompatible with the specified collection
     * @throws NullElementException if this set contains a null element and the
     *         specified collection does not permit null elements
     *         or if the specified elements is null
     */
    public function retainAll(iterable $elements): bool
    {
        $collection = ($elements instanceof Collection) ? $elements : new MixedSequence($elements);

        $newElements = [];
        foreach ($this->elements as $element) {
            if ($collection->contains($element)) {
                $newElements[] = $element;
            }
        }

        $changed = (count($newElements) - count($this->elements)) !== 0;
        $this->elements = $newElements;

        return $changed;
    }

    /**
     * Compares the specified object with this set for equality.  Returns
     * true if the specified object is also a set, the two sets
     * have the same size, and every member of the specified set is
     * contained in this set (or equivalently, every member of this set is
     * contained in the specified set).  This definition ensures that the
     * equals method works properly across different implementations of the
     * set interface.
     *
     * @param Set<TValue> $set to be compared for equality with this set
     * @return bool true if the specified object is equal to this set
     */
    public function equals(Set $set): bool
    {
        try {
            $result = $this->count() === $set->count() && $this->containsAll($set);
        } catch (InvalidElementException $exception) {
            $result = false;
        } catch (NullElementException $exception) {
            $result = false;
        }

        return $result;
    }
}
