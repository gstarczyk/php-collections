<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;

/**
 * Not nullable sequence of scalars
 * @template TValue of scalar
 * @extends AbstractSequence<TValue>
 * @implements Sequence<TValue>
 */
class ScalarsSequence extends AbstractSequence implements Sequence
{
    /**
     * @inheritdoc
     */
    protected function validateElement(mixed $element): void
    {
        if (null === $element) {
            throw new NullElementException('Element cannot be NULL');
        }
        if (!is_scalar($element)) {
            throw InvalidElementException::invalidType('scalar', $element);
        }
    }
}
