<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\IndexOutOfBoundsException;

/**
 * @template TValue
 * @implements Sequence<TValue>
 * @extends AbstractCollection<TValue>
 */
abstract class AbstractSequence extends AbstractCollection implements Sequence
{
    /**
     * @inheritdoc
     */
    public function subSequence(int $fromIndex, int $toIndex): Sequence
    {
        return new SubSequence($this, $fromIndex, $toIndex);
    }

    /**
     * @inheritdoc
     */
    public function add(mixed $element): bool
    {
        $this->validateElement($element);
        $this->elements[] = $element;

        return true;
    }

    /**
     * @inheritdoc
     */
    public function addAtIndex(int $index, mixed $element): void
    {
        $this->validateElement($element);
        $this->validateIndex($index);

        $head = array_slice($this->elements, 0, $index);
        $tail = array_slice($this->elements, $index);
        $head[] = $element;

        $this->elements = array_merge($head, $tail);
    }

    /**
     * @inheritdoc
     */
    public function addAllAtIndex(int $index, iterable $elements): bool
    {
        $this->validateElements($elements);
        $this->validateIndex($index);

        $head = array_slice($this->elements, 0, $index);
        $tail = array_slice($this->elements, $index);
        foreach ($elements as $element) {
            $head[] = $element;
        }

        $this->elements = array_merge($head, $tail);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function contains(mixed $element): bool
    {
        return ($this->indexOf($element) >= 0);
    }

    /**
     * @inheritdoc
     */
    public function get(int $index): mixed
    {
        $this->validateIndex($index);

        return $this->elements[$index];
    }

    /**
     * @inheritdoc
     */
    public function indexOf(mixed $element): int
    {
        $index = array_search($element, $this->elements);

        return ($index === false) ? -1 : (int)$index;
    }

    /**
     * @inheritdoc
     */
    public function lastIndexOf(mixed $element): int
    {
        $lastIndex = -1;
        foreach ($this->elements as $index => $elem) {
            if ($elem == $element) {
                $lastIndex = $index;
            }
        }

        return $lastIndex;
    }

    /**
     * @inheritdoc
     */
    public function removeIndex(int $index): mixed
    {
        $this->validateIndex($index);
        $removedElement = $this->get($index);
        $head = array_slice($this->elements, 0, $index);
        $tail = array_slice($this->elements, $index + 1);
        $this->elements = array_merge($head, $tail);

        return $removedElement;
    }

    /**
     * @inheritdoc
     */
    public function remove(mixed $element): bool
    {
        $index = $this->indexOf($element);
        if ($index >= 0) {
            $this->removeIndex($index);

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function retainAll(iterable $elements): bool
    {
        if ($elements instanceof Collection) {
            $collection = $elements;
        } else {
            $collection = new MixedSequence();
            $collection->addAll($elements);
        }
        $changed = false;
        $newElements = [];
        foreach ($this->elements as $element) {
            if ($collection->contains($element)) {
                $newElements[] = $element;
            }
        }
        if (count($this->elements) !== count($newElements)) {
            $changed = true;
        }
        $this->elements = $newElements;

        return $changed;
    }

    /**
     * @inheritdoc
     */
    public function set(int $index, mixed $element): mixed
    {
        $this->validateIndex($index);
        $this->validateElement($element);
        $replacedElement = $this->get($index);
        $this->elements[$index] = $element;

        return $replacedElement;
    }

    /**
     * @inheritdoc
     */
    public function count(): int
    {
        return count($this->elements);
    }

    /**
     * @inheritdoc
     */
    public function sort(Comparator $comparator): void
    {
        usort($this->elements, [$comparator, '__invoke']);
    }

    /**
     * @inheritdoc
     */
    public function toArray(): array
    {
        return $this->elements;
    }

    /**
     * Check if given index is integer and is not out of bounds
     *
     * @param int $index
     * @throws IndexOutOfBoundsException if the index is out of range
     */
    protected function validateIndex(int $index): void
    {
        if ($index < 0 || $index >= $this->count()) {
            throw new IndexOutOfBoundsException(
                sprintf(
                    'Invalid sequence index. Allowed range (%u, %u), but %u was given.',
                    0,
                    $this->count(),
                    $index
                )
            );
        }
    }
}
