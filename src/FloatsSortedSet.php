<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;

/**
 * @extends AbstractSortedSet<float>
 * @implements  SortedSet<float>
 */
class FloatsSortedSet extends AbstractSortedSet implements SortedSet
{
    /**
     * @inheritDoc
     */
    protected function validateElement(mixed $element): void
    {
        if ($element === null) {
            throw new NullElementException('FloatsSortedSet does not accept null elements.');
        }
        if (!is_float($element)) {
            throw InvalidElementException::invalidType('float', $element);
        }
    }

    protected function createDefaultComparator(): Comparator
    {
        return new CallbackComparator(
            function ($element1, $element2) {
                $result = $element1 - $element2;
                if ($result > 0) {
                    return 1;
                } elseif ($result < 0) {
                    return -1;
                }

                return 0;
            }
        );
    }
}
