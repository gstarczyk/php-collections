<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;

/**
 * @extends AbstractSortedSet<int>
 * @implements SortedSet<int>
 */
class IntegersSortedSet extends AbstractSortedSet implements SortedSet
{
    /**
     * @inheritDoc
     */
    protected function validateElement(mixed $element): void
    {
        if ($element === null) {
            throw new NullElementException('IntegersSortedSet does not accept null elements.');
        }
        if (!is_integer($element)) {
            throw InvalidElementException::invalidType('integer', $element);
        }
    }

    /**
     * @inheritdoc
     */
    protected function createDefaultComparator(): Comparator
    {
        $comparator = new CallbackComparator(
            function ($element1, $element2) {
                return $element1 - $element2;
            }
        );

        return $comparator;
    }
}
