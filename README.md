# PHP collections framework

Missing features of PHP core or SPL collections.

This lib try to add as much as can from Java Collections Framework to PHP world.

## Sequence

An ordered collection (also known as a list).

The user of this interface has precise control over where in the list each element is inserted. 
The user can access elements by their integer index (position in the list), 
and search for elements in the list.

### Usage

You can count elements in set
```php
$count = $sequence->count();
```

You can remove all the elements from set
```php
$sequence->clear();
``` 

You can check if set contains any element
```php
$sequence->isEmpty();
```

You can add one element to the end of sequence:

```php
$added = $sequence->add($element);
if ($added) {
    echo 'element is added';
} else {
    echo 'element is not added to sequence';
}
```

or collection of elements (added elements will be appended to the end of sequence):

```php
$elements = [
    $element1,
    $element2,
    $element3,
];
$sequence->addAll($elements);
```

You can add element at specific index, this will shift the element currently at that position (if any) 
and any subsequent elements to the right (adds one to their indices):

```php
$sequence->addAtIndex($index, $element);
```

You can even add a collection of elements at specified index. 
Shifts the element currently at that position (if any) 
and any subsequent elements to the right (increases their indices):

```php
$sequence->addAllAtIndex($index, $elements);
```

You can remove one element (only first occurrence):

```php
$changed = $sequence->remove($element);
if ($changed) {
    echo 'element removed';
} else {
    echo 'element not found in sequence';
}
```

or remove all occurrences of element:
    
```php
$changed = $sequence->removeAll([$element]);
if ($changed) {
    echo 'element is removed from sequence';
} else {
    echo 'element is not found in sequence';
}
```

or even remove all occurrences of collection of elements:

```php
$changed = $sequence->removeAll([
    $element1,
    $element2
]);
if ($changed) {
    echo 'at least one from given elements is removed from sequence';
} else {
    echo 'none of given elements are not found in sequence';
}
```

You can replace element at specified index:

```php
$oldElement = $sequence->set(1, $element);
```

You can remove all elements from sequence:

```php
$sequence->clear();
```

If you need you can sort this sequence according to the order induced by the specified Comparator.

```php
$sequence->sort(Comparator $comparator);
```

And you can operate on some part of sequence by getting sub-sequence:

```php
$subSequence = $sequence->subSequence($fromIndex, $toIndex);
```
    
but remember sub-sequences are still part of main sequence, 
so making changes on sub-sequence you making changes on sequence:

```php
$subSequence = $sequence->subSequence($fromIndex, $toIndex);
$subSequence->remove($element);

$found = $sequence->contains($element); 
//$found will be false as we remove $element from $sequence by removing it from $subSequence
```

### Provided implementations of Sequence

You can use sequence for any type of elements but if you need sequence of certain type of elements
you can use specialized type of sequence.

#### MixedSequence

This type of sequence accept all types of elements except of null elements.

```php
$mySequence = new MixedSequence();
```

#### ScalarsSequence

This type of sequence accept only scalar elements.
Null elements are not allowed.

```php
$mySequence = new ScalarsSequence();
```

#### IntegersSequence

This type of sequence accept only integer elements.
Null elements are not allowed.

```php
$mySequence = new IntegersSequence();
```

#### StringsSequence

This type of sequence accept only string elements.
Null elements are not allowed.

```php
$mySequence = new StringsSequence();
```

#### ObjectsSequence

This type of sequence accept only elements being instances of specified class.
Null elements are not allowed.

```php
$mySequence = new ObjectsSequence(SequenceElement::class);
```

## Set

An collection of unique elements.

### Usage

You can count elements in set

```php
$count = $set->count();
```

You can remove all the elements from set

```php
$set->clear();
```
    
You can check if set contains any element

```php
$set->isEmpty();
```

Adding element will return boolean: true - if element is added, false if element could not be added 
(same element exist in set).

```php
$added = $set->add($element);
if ($added) {
    echo 'element is added';
} else {
    echo 'element exist in set and cannot be added';
}
```

You can add multiple elements

```php
$modified = $set->addAll([$element1, $element2]);
if ($modified) {
    echo 'some of given elements are added to set';
} else {
    echo 'non of given elements are added to set';
}
```

You can check if set contains element

```php
$contains = $set->contains($element);
if ($contains) {
    echo 'set contains specified element';
} else {
    echo 'set do not contains specified element';
}
```

or check if set contains all given elements

```php
$contains = $set->contains([$element1, $element2, element3]);
if ($contains) {
    echo 'set contains element1 and element2 and element3';
} else {
    echo 'set do not contains some of given elements or even any element from given collection';
}
```

You can remove element from set

```php
$removed = $set->remove($element);
if ($removed) {
    echo "given element removed from set";
} else {
    echo "set do not contains given element";
}
```

or remove collection of elements

```php
$changed = $set->removeAll([$element1, $element2]);
if ($changed) {
    echo "Some or all elements from given collection are removed from set";
} else {
    echo "None element from given collection was found in set";
}
```

or even remove all elements from set that are not present in given collection

```php
$changed = $set->retainAll([$element1, $element2]);
if ($changed) {
    echo "All elements that are not present in given collection are removed from set";
} else {
    echo "None element id removed from set, so this set and given collection has same elements";
}
```

You can check is two sets are equal

```php
if ($set1->equals($set2) {
    echo 'given sets are equal'
} else {
    echo 'given sets are not equal'
}
```

If you need operate on set as on array to can convert it

```php
$array = $set->toArray();
```

### Provided implementations of Set

#### IntegersSet

This type of set accepts only integer elements.
Null elements are not allowed.

```php
$mySet = new IntegersSet();
```

#### FloatsSet

This type of set accepts only float elements.
Null elements are not allowed.

```php
$mySet = new FloatsSet();
```

#### StringsSet

This type of set accepts only string elements.
Null elements are not allowed.

```php
$mySet = new StringsSet();
```

#### ObjectsSet

This type of set accepts only elements being instance of specified class.
Null elements are not allowed.

```php
$mySet = new ObjectsSet(SetElement::class);
```

## SortedSet

A Set that further provides a total ordering on its elements.

### Usage

SortedSet contains all features provided by Set, and adds a couple of our features.
All elements are sorted by required Comparator implementation (implementations for strings, integers and floats
contains default comparators that sorts elements by natural order).

Added features are:
* get first and last elements
* get set view (subset)

#### Obtaining first and last elements
    
```php
$first = $mySortedSet->first();
$last = $mySortedSet->last();
```

#### Creating subsets (set views)

There are 3 methods for creating subset

```php
$head = $mySortedSet->headSet($toElement); // where $toElement is exclusive
$tail = $mySortedSet->tailSet($fromElement); //where $fromElement is inclusive
$subSet = $mySortedSet->subSet($fromElement, $toElement); // same rules as above
``` 
      
but remember subsets are still part of main set, 
so making changes on subset you are making changes on parent set and vice versa.

So you can:

```php
$subset = $mySet->subSet($fromElement, $toElement);
$subset->clear();
```

and then all elements from `$fromElement` inclusive to `$toElement` exclusive are removed from `$subset` and `$mySet`
which means that `$subset` is now empty Set.

To be more clear:

```php
$mySet->addAll([
    $elem01,
    $elem03,
    $elem10,
]);
$subset = $mySet->subset($elem01,$elem10);
// $subset contains now $elem01, $elem03

$subset->add($elem02);
//subset contains now $elem01, $elem02, $elem03
//$mySet contains now $elem01, $elem02, $elem03, $elem10

$mySet->add($elem07);
//subset contains now $elem01, $elem02, $elem03, $elem07
//$mySet contains now $elem01, $elem02, $elem03, $elem07, $elem10

$mySet->add($elem11);
//subset contains now $elem01, $elem02, $elem03, $elem07
//$mySet contains now $elem01, $elem02, $elem03, $elem07, $elem10, $elem11
```

If you need static subset, just do (assuming you want strings set):

```php
$mySet = new StringsSortedSet();
//...
$subset = $mySet->subset($elem01,$elem10);
$myStaticSubSet = new StringsSortedSet($subset);
```

### Provided implementations of SortedSet

#### IntegersSortedSet

This type of set accepts only integer elements.
Null elements are not allowed.

```php
$mySet = new IntegersSortedSet();
// or
$mySet = new IntegersSortedSet($comparator);
```

#### FloatsSortedSet

This type of set accepts only float elements.
Null elements are not allowed.

```php
$mySet = new FloatsSortedSet();
// or
$mySet = new FloatsSortedSet($comparator);
```

#### StringsSortedSet

This type of set accepts only string elements.
Null elements are not allowed.

```php
$mySet = new StringsSortedSet();
// or
$mySet = new StringsSortedSet($comparator);
```

#### ObjectsSortedSet

This type of set accepts only elements being instance of specified class.
Null elements are not allowed.

```php
$mySet = new ObjectsSortedSet(SetElement::class);
// or
$mySet = new ObjectsSortedSet(SetElement::class, $comparator);
```
