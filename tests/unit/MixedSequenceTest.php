<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\CallbackComparator;
use Gstarczyk\PhpCollections\Comparator;
use Gstarczyk\PhpCollections\MixedSequence;
use Gstarczyk\PhpCollections\Sequence;
use PHPUnit\Framework\Attributes\CoversClass;

/**
 * @extends AbstractSequenceTestCase<mixed>
 */
#[CoversClass(MixedSequence::class)]
class MixedSequenceTest extends AbstractSequenceTestCase
{
    private static int $elementId = 0;

    protected function setUp(): void
    {
        self::$elementId = 0;
        parent::setUp();
    }

    protected static function createValidElement(): mixed
    {
        $validElements = [
            1,
            2,
            'aa3',
            'bb4',
            6.5,
            [8],
            new \stdClass(),
        ];

        if (array_key_exists(self::$elementId, $validElements)) {
            $element = $validElements[self::$elementId];
        } else {
            $element = self::$elementId;
        }
        ++self::$elementId;

        return $element;
    }

    protected static function createInvalidElement(): mixed
    {
        self::markTestSkipped('Cannot test invalid element as MixedSequence accept all types.');
    }

    /**
     * @return Sequence<mixed>
     */
    protected static function createEmptySequence(): Sequence
    {
        return new MixedSequence();
    }

    /**
     * @return Comparator<mixed>
     */
    protected function createComparator(): Comparator
    {
        return new CallbackComparator(
            function ($item1, $item2) {
                if (is_int($item1) && is_int($item2)) {
                    return $item1 - $item2;
                } elseif (is_scalar($item1) && is_scalar($item2)) {
                    return strcmp((string)$item1, (string)$item2);
                } elseif (is_scalar($item1)) {
                    return -1;
                } elseif (is_scalar($item2)) {
                    return 1;
                }

                return -1;
            }
        );
    }
}
