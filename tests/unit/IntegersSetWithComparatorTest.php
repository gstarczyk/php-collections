<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\CallbackComparator;
use Gstarczyk\PhpCollections\IntegersSet;
use Gstarczyk\PhpCollections\Set;
use PHPUnit\Framework\Attributes\CoversClass;
use stdClass;

#[CoversClass(IntegersSet::class)]
class IntegersSetWithComparatorTest extends AbstractSetTestCase
{
    /**
     * @return Set<int>
     */
    protected static function createEmptySet(): Set
    {
        $comparator = new CallbackComparator(
            function ($element1, $element2) {
                return $element1 - $element2;
            }
        );

        return new IntegersSet($comparator);
    }

    protected static function createValidElement(float $id): int
    {
        return (int)($id * 10);
    }

    /**
     * @return array<int, array<int, mixed>>
     */
    public static function invalidElementsProvider(): array
    {
        return [
            ['text'],
            [5.5],
            [[]],
            [new stdClass()],
        ];
    }
}
