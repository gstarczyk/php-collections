<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\CallbackComparator;
use Gstarczyk\PhpCollections\ObjectsSortedSet;
use Gstarczyk\PhpCollections\Set;
use Gstarczyk\PhpCollections\TestFixture\CollectionElement;
use PHPUnit\Framework\Attributes\CoversClass;
use stdClass;

#[CoversClass(ObjectsSortedSet::class)]
class ObjectsSortedSetTest extends AbstractSortedSetTestCase
{
    /**
     * @return Set<CollectionElement>
     */
    protected static function createEmptySet(): Set
    {
        $comparator = new CallbackComparator(
            function (CollectionElement $element1, CollectionElement $element2) {
                return strcmp($element1->name, $element2->name);
            }
        );

        return new ObjectsSortedSet(CollectionElement::class, $comparator);
    }

    protected static function createValidElement(float $id): CollectionElement
    {
        return new CollectionElement('element#' . $id);
    }

    /**
     * @return array<int, array<int, mixed>>
     */
    public static function invalidElementsProvider(): array
    {
        return [
            [1],
            ['text'],
            [1.5],
            [new stdClass()],
            [[]],
        ];
    }

    /**
     * @return array<string, array<int, mixed>>
     */
    public static function invalidSubSetBoundariesProvider(): array
    {
        return [
            'invalid from' => ['invalid', static::createValidElement(10.0)],
            'invalid to' => [static::createValidElement(1.0), 10.0],
            'invalid both' => [1, 10],
        ];
    }
}
