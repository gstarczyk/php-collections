<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\CallbackComparator;
use Gstarczyk\PhpCollections\Exceptions\IndexOutOfBoundsException;
use Gstarczyk\PhpCollections\ObjectsSequence;
use Gstarczyk\PhpCollections\Sequence;
use Gstarczyk\PhpCollections\SubSequence;
use Gstarczyk\PhpCollections\TestFixture\CollectionElement;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

#[CoversClass(SubSequence::class)]
class SubSequenceTest extends TestCase
{
    private const FROM_INDEX = 1;
    private const TO_INDEX = 4;
    private const INITIAL_SIZE = 3;

    /**
     * @var SubSequence<CollectionElement>
     */
    private SubSequence $subSequence;

    /**
     * @var Sequence<CollectionElement>
     */
    private Sequence $parentSequence;

    protected function setUp(): void
    {
        $this->parentSequence = new ObjectsSequence(CollectionElement::class);
        $this->parentSequence->addAll(
            [
                new CollectionElement('one'),
                new CollectionElement('two'),
                new CollectionElement('three'),
                new CollectionElement('two'),
                new CollectionElement('four'),
            ]
        );
        $this->subSequence = new SubSequence($this->parentSequence, self::FROM_INDEX, self::TO_INDEX);
    }

    public function testGetIterator(): void
    {
        $iterator = $this->subSequence->getIterator();

        $elements = iterator_to_array($iterator);
        $expectedElements = [
            new CollectionElement('two'),
            new CollectionElement('three'),
            new CollectionElement('two'),
        ];
        Assert::assertEquals($expectedElements, $elements);
    }

    public function testToArrayReturnArrayContainingAllElements(): void
    {
        $expectedElements = [
            new CollectionElement('two'),
            new CollectionElement('three'),
            new CollectionElement('two'),
        ];

        Assert:
        self::assertEquals($expectedElements, $this->subSequence->toArray());
    }

    public function testInitialCountIsCalculatedFromIndexRange(): void
    {
        $result = $this->subSequence->count();

        $expectedSize = self::TO_INDEX - self::FROM_INDEX;
        Assert::assertEquals($expectedSize, $result);
    }

    public function testAddIncreaseSizeByOne(): void
    {
        $sizeBefore = $this->subSequence->count();

        $this->subSequence->add(new CollectionElement('one'));
        $sizeAfter = $this->subSequence->count();

        Assert::assertEquals($sizeBefore + 1, $sizeAfter);
    }

    public function testAdd(): void
    {
        $element = new CollectionElement('one-prim');
        $this->subSequence->add($element);

        $expectedElements = [
            new CollectionElement('two'),
            new CollectionElement('three'),
            new CollectionElement('two'),
            $element,
        ];
        $expectedParentElements = [
            new CollectionElement('one'),
            new CollectionElement('two'),
            new CollectionElement('three'),
            new CollectionElement('two'),
            $element,
            new CollectionElement('four'),
        ];
        Assert::assertEquals($expectedElements, $this->subSequence->toArray());
        Assert::assertEquals($expectedParentElements, $this->parentSequence->toArray());
    }

    public function testAddAtIndexIncreaseSizeByOne(): void
    {
        $sizeBefore = $this->subSequence->count();

        $this->subSequence->addAtIndex(1, new CollectionElement('one'));
        $sizeAfter = $this->subSequence->count();

        Assert::assertEquals($sizeBefore + 1, $sizeAfter);
    }

    public function testAddAtIndexThrowExceptionIfSpecifiedIndexIsBelowZero(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->subSequence->addAtIndex(-1, new CollectionElement('one'));
    }

    public function testAddAtIndexThrowExceptionIfSpecifiedIndexIsEqualToMaxIndexOrGreater(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->subSequence->addAtIndex(self::INITIAL_SIZE, new CollectionElement('one'));
    }

    public function testAddAtIndex(): void
    {
        $element = new CollectionElement('one-prim');
        $this->subSequence->addAtIndex(1, $element);

        $expectedElements = [
            new CollectionElement('two'),
            $element,
            new CollectionElement('three'),
            new CollectionElement('two'),
        ];
        $expectedParentElements = [
            new CollectionElement('one'),
            new CollectionElement('two'),
            $element,
            new CollectionElement('three'),
            new CollectionElement('two'),
            new CollectionElement('four'),
        ];
        Assert::assertEquals($expectedElements, $this->subSequence->toArray());
        Assert::assertEquals($expectedParentElements, $this->parentSequence->toArray());
    }

    public function testAddAllIncreaseSizeByCountOfAddedElements(): void
    {
        $sizeBefore = $this->subSequence->count();

        $elements = [
            new CollectionElement('one'),
            new CollectionElement('two'),
        ];
        $this->subSequence->addAll($elements);
        $sizeAfter = $this->subSequence->count();

        Assert::assertEquals($sizeBefore + 2, $sizeAfter);
    }

    public function testAddAll(): void
    {
        $elements = [
            new CollectionElement('one-prim'),
            new CollectionElement('two-prim'),
        ];
        $this->subSequence->addAll($elements);

        $expectedElements = [
            new CollectionElement('two'),
            new CollectionElement('three'),
            new CollectionElement('two'),
            $elements[0],
            $elements[1],
        ];
        $expectedParentElements = [
            new CollectionElement('one'),
            new CollectionElement('two'),
            new CollectionElement('three'),
            new CollectionElement('two'),
            $elements[0],
            $elements[1],
            new CollectionElement('four'),
        ];
        Assert::assertEquals($expectedElements, $this->subSequence->toArray());
        Assert::assertEquals($expectedParentElements, $this->parentSequence->toArray());
    }

    public function testAddAllAtIndexIncreaseSizeByCountOfAddedElements(): void
    {
        $sizeBefore = $this->subSequence->count();

        $elements = [
            new CollectionElement('one'),
            new CollectionElement('two'),
        ];
        $this->subSequence->addAllAtIndex(1, $elements);
        $sizeAfter = $this->subSequence->count();

        Assert::assertEquals($sizeBefore + 2, $sizeAfter);
    }

    public function testAddAllAtIndexThrowExceptionIfSpecifiedIndexIsBelowZero(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->subSequence->addAllAtIndex(-1, [new CollectionElement('one')]);
    }

    public function testAddAllAtIndexThrowExceptionIfSpecifiedIndexIsEqualToMaxIndexOrGreater(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->subSequence->addAllAtIndex(self::INITIAL_SIZE, [new CollectionElement('one')]);
    }

    public function testAddAllAtIndex(): void
    {
        $elements = [
            new CollectionElement('one-prim'),
            new CollectionElement('two-prim'),
        ];
        $this->subSequence->addAllAtIndex(1, $elements);

        $expectedElements = [
            new CollectionElement('two'),
            $elements[0],
            $elements[1],
            new CollectionElement('three'),
            new CollectionElement('two'),
        ];
        $expectedParentElements = [
            new CollectionElement('one'),
            new CollectionElement('two'),
            $elements[0],
            $elements[1],
            new CollectionElement('three'),
            new CollectionElement('two'),
            new CollectionElement('four'),
        ];
        Assert::assertEquals($expectedElements, $this->subSequence->toArray());
        Assert::assertEquals($expectedParentElements, $this->parentSequence->toArray());
    }

    public function testClearRemovesAllElementsFromParentSequence(): void
    {
        $this->subSequence->clear();

        $expectedElements = [
            new CollectionElement('one'),
            new CollectionElement('four'),
        ];
        Assert::assertEquals($expectedElements, $this->parentSequence->toArray());
    }

    public function testClearSetSizeToZero(): void
    {
        $this->subSequence->clear();
        $result = $this->subSequence->count();

        Assert::assertEquals(0, $result);
    }

    public function testContainsReturnTrueIfSpecifiedElementExistInParentSequenceWithinIndexRange(): void
    {
        $result = $this->subSequence->contains(new CollectionElement('two'));

        Assert::assertTrue($result);
    }

    public function testContainsReturnFalseIfSpecifiedElementNotExistInParentSequenceWithinIndexRange(): void
    {
        $result = $this->subSequence->contains(new CollectionElement('one'));

        Assert::assertFalse($result);
    }

    public function testContainsAllReturnTrueWhenAllSpecifiedElementsAreExistInCollection(): void
    {
        $result = $this->subSequence->containsAll(
            [
                new CollectionElement('two'),
                new CollectionElement('three'),
            ]
        );

        Assert::assertTrue($result);
    }

    public function testContainsAllReturnFalseWhenAnyOfSpecifiedElementsIsNotExistInCollection(): void
    {
        $result = $this->subSequence->containsAll(
            [
                new CollectionElement('one'),
                new CollectionElement('three'),
            ]
        );

        Assert::assertFalse($result);
    }

    public function testGetReturnElementStoredAtSpecifiedIndex(): void
    {
        $result = $this->subSequence->get(1);

        $expectedElement = $this->parentSequence->get(2);
        Assert::assertSame($expectedElement, $result);
    }

    public function testGetThrowExceptionWhenSpecifiedIndexIsLowerThanZero(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->subSequence->get(-1);
    }

    public function testGetThrowExceptionWhenSpecifiedIndexIsGreaterThanMaxIndexInThisSequence(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->subSequence->get(self::INITIAL_SIZE);
    }

    public function testEmptyReturnFalseIfCollectionContainsElements(): void
    {
        $result = $this->subSequence->isEmpty();

        Assert::assertFalse($result);
    }

    public function testEmptyReturnTrueIfCollectionDoesNotContainsAnyElement(): void
    {
        $this->subSequence->clear();

        $result = $this->subSequence->isEmpty();

        Assert::assertTrue($result);
    }

    public function testLastIndexOfReturnIndexOfLastOccurrenceSpecifiedElement(): void
    {
        $element = new CollectionElement('two');
        $this->subSequence->add($element);

        $result = $this->subSequence->lastIndexOf($element);

        Assert::assertSame(3, $result);
    }

    public function testLastIndexOfReturnMinusOneIfListDoesNotContainsSpecifiedElement(): void
    {
        $result = $this->subSequence->lastIndexOf(new CollectionElement('four'));

        Assert::assertSame(-1, $result);
    }

    public function testRemoveIndexRemovesElementFromSelfAndParentSequence(): void
    {
        $this->subSequence->removeIndex(2);

        $expectedElements = [
            new CollectionElement('two'),
            new CollectionElement('three'),
        ];
        $expectedParentElements = [
            new CollectionElement('one'),
            new CollectionElement('two'),
            new CollectionElement('three'),
            new CollectionElement('four'),
        ];

        Assert::assertEquals($expectedElements, $this->subSequence->toArray());
        Assert::assertEquals($expectedParentElements, $this->parentSequence->toArray());
    }

    public function testRemoveDetachFirstOccurrenceSpecifiedElement(): void
    {
        $this->subSequence->remove(new CollectionElement('two'));

        $expectedElements = [
            new CollectionElement('three'),
            new CollectionElement('two'),
        ];
        $expectedParentElements = [
            new CollectionElement('one'),
            new CollectionElement('three'),
            new CollectionElement('two'),
            new CollectionElement('four'),
        ];
        Assert::assertEquals($expectedElements, $this->subSequence->toArray());
        Assert::assertEquals($expectedParentElements, $this->parentSequence->toArray());
    }

    public function testRemoveAllRemovesAllOccurrencesOfSpecifiedElements(): void
    {
        $this->subSequence->removeAll([new CollectionElement('two')]);

        $expectedElements = [
            new CollectionElement('three'),
        ];
        Assert::assertEquals($expectedElements, $this->subSequence->toArray());
        $expectedParentElements = [
            new CollectionElement('one'),
            new CollectionElement('three'),
            new CollectionElement('four'),
        ];
        Assert::assertEquals($expectedParentElements, $this->parentSequence->toArray());
    }

    public function testRetainAllRemovesAllElementsThatAreNotExistsInSpecifiedCollection(): void
    {
        $collection = new ObjectsSequence(CollectionElement::class);
        $collection->addAll([new CollectionElement('three')]);
        $this->subSequence->retainAll($collection);
        $expectedElements = [
            new CollectionElement('three'),
        ];
        Assert::assertEquals($expectedElements, $this->subSequence->toArray());
        $expectedParentElements = [
            new CollectionElement('one'),
            new CollectionElement('three'),
            new CollectionElement('four'),
        ];
        Assert::assertEquals($expectedParentElements, $this->parentSequence->toArray());
    }

    #[DataProvider('outOfRangeIndexProvider')]
    public function testSetThrowExceptionWhenIndexIsOutOfAllowedRange(int $index): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->subSequence->set($index, new CollectionElement('new'));
    }

    public function testSetReplaceElementAtSpecifiedIndex(): void
    {
        $element = new CollectionElement('new');
        $this->subSequence->set(1, $element);

        $expectedElements = [
            new CollectionElement('two'),
            new CollectionElement('new'),
            new CollectionElement('two'),
        ];

        Assert::assertEquals($expectedElements, $this->subSequence->toArray());
    }

    public function testSort(): void
    {
        $this->subSequence->add(new CollectionElement('aaaa'));
        $this->subSequence->sort(
            new CallbackComparator(
                function (CollectionElement $item1, CollectionElement $item2) {
                    $name1 = $item1->name;
                    $name2 = $item2->name;
                    if ($name1 > $name2) {
                        return 1;
                    } elseif ($name1 == $name2) {
                        return 0;
                    } else {
                        return -1;
                    }
                }
            )
        );
        $expectedElements = [
            new CollectionElement('aaaa'),
            new CollectionElement('three'),
            new CollectionElement('two'),
            new CollectionElement('two'),
        ];

        Assert::assertEquals($expectedElements, $this->subSequence->toArray());
    }

    public function testSubSequence(): void
    {
        $result = $this->subSequence->subSequence(1, 2);

        $expectedElements = [
            new CollectionElement('three'),
        ];

        Assert::assertEquals($expectedElements, $result->toArray());
    }

    /**
     * @return int[][]
     */
    public static function outOfRangeIndexProvider(): array
    {
        return [
            [-1],
            [self::INITIAL_SIZE],
            [1000],
        ];
    }
}
