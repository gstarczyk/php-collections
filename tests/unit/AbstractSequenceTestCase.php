<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\Comparator;
use Gstarczyk\PhpCollections\Exceptions\IndexOutOfBoundsException;
use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;
use Gstarczyk\PhpCollections\Sequence;
use Gstarczyk\PhpCollections\SubSequence;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

/**
 * @template TValue
 */
abstract class AbstractSequenceTestCase extends TestCase
{
    /**
     * @var Sequence<TValue>
     */
    protected Sequence $sequence;

    protected function setUp(): void
    {
        $this->sequence = static::createEmptySequence();
    }

    abstract protected static function createValidElement(): mixed;

    abstract protected static function createInvalidElement(): mixed;

    /**
     *
     * @return Sequence<TValue>
     */
    abstract protected static function createEmptySequence(): Sequence;

    public function testAddAppendGivenElementToSequence(): void
    {
        $elem1 = static::createValidElement();
        $elem2 = static::createValidElement();
        $elem3 = static::createValidElement();
        $this->sequence->addAll([$elem1, $elem2, $elem3]);

        $expectedResult = [
            $elem1,
            $elem2,
            $elem3,
        ];
        Assert::assertEquals($expectedResult, $this->sequence->toArray());
    }

    public function testAddThrowExceptionWhenNullElementWasGiven(): void
    {
        $this->expectException(NullElementException::class);
        /** @phpstan-ignore-next-line */
        $this->sequence->add(null);
    }

    public function testAddThrowExceptionWhenInvalidElementWasGiven(): void
    {
        $this->expectException(InvalidElementException::class);

        $this->sequence->add(static::createInvalidElement());
    }

    public function testAddAtIndexInsertGivenElementAtSpecifiedIndexShiftingRightElementsByOne(): void
    {
        $elem1 = static::createValidElement();
        $elem2 = static::createValidElement();
        $elem3 = static::createValidElement();
        $this->sequence->addAll([$elem1, $elem2, $elem3]);

        $elem4 = static::createValidElement();
        $this->sequence->addAtIndex(1, $elem4);

        $expectedResult = [
            $elem1,
            $elem4,
            $elem2,
            $elem3,
        ];
        Assert::assertEquals($expectedResult, $this->sequence->toArray());
    }

    public function testAddAtIndexThrowExceptionWhenIndexLowerThenZeroWasGiven(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->sequence->addAtIndex(-2, $this->createValidElement());
    }

    public function testAddAtIndexThrowExceptionWhenIndexGreaterThenSequenceLengthWasGiven(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->sequence->addAtIndex(2, $this->createValidElement());
    }

    public function testAddAllAppendSpecifiedElementsToCollection(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $element3 = static::createValidElement();
        $element4 = static::createValidElement();
        $this->sequence->add($element1);

        $this->sequence->addAll(
            [
                $element2,
                $element3,
                $element4,
            ]
        );

        $expectedElements = [
            $element1,
            $element2,
            $element3,
            $element4,
        ];
        Assert::assertEquals($expectedElements, $this->sequence->toArray());
    }

    public function testAddAllReturnTrueIfCollectionChangeAfterThisCall(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $elements = [
            $element1,
            $element2,
        ];

        $result = $this->sequence->addAll($elements);

        Assert::assertTrue($result);
    }

    public function testAddAllAtIndexInsertElementsAtGivenIndexShiftingRightElementsBySizeOfInsertedCollection(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $this->sequence->addAll([$element1, $element2]);

        $element3 = static::createValidElement();
        $element4 = static::createValidElement();
        $this->sequence->addAllAtIndex(1, [$element3, $element4]);

        $expectedResult = [
            $element1,
            $element3,
            $element4,
            $element2,
        ];
        Assert::assertEquals($expectedResult, $this->sequence->toArray());
    }

    public function testClearMakeSequenceEmpty(): void
    {
        $this->sequence->add($this->createValidElement());

        $this->sequence->clear();
        Assert::assertCount(0, $this->sequence->toArray());
    }

    public function testAddAllAppendAllGivenElementsToSequence(): void
    {
        $elem1 = static::createValidElement();
        $elem2 = static::createValidElement();
        $elem3 = static::createValidElement();
        $this->sequence->addAll([$elem1, $elem2, $elem3]);

        $expectedResult = [$elem1, $elem2, $elem3];
        Assert::assertEquals($expectedResult, $this->sequence->toArray());
    }

    public function testGetReturnElementStoredAtSpecifiedIndex(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $this->sequence->add($element1);
        $this->sequence->add($element2);

        $result = $this->sequence->get(1);
        Assert::assertSame($element2, $result);
    }

    public function testGetThrowExceptionWhenSpecifiedIndexIsLowerThanZero(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->sequence->get(-1);
    }

    public function testGetThrowExceptionWhenSpecifiedIndexIsGreaterThanMaxIndexInThisSequence(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->sequence->get(2);
    }

    public function testIsEmptyReturnTrueForSequenceWithoutElements(): void
    {
        $result = $this->sequence->isEmpty();

        Assert::assertTrue($result);
    }

    public function testIsEmptyReturnFalseForSequenceWithAtLeastOneElement(): void
    {
        $this->sequence->add($this->createValidElement());
        $result = $this->sequence->isEmpty();

        Assert::assertFalse($result);
    }

    /**
     * @param array<int, mixed> $elements
     * @param array<int, mixed> $expected
     */
    #[DataProvider('removeIndexDataProvider')]
    public function testRemoveIndexDropSpecifiedIndexAndCloseGap(
        array $elements,
        int $indexToRemove,
        array $expected
    ): void {
        $this->sequence->addAll($elements);

        $this->sequence->removeIndex($indexToRemove);

        Assert::assertEquals($expected, $this->sequence->toArray());
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public static function removeIndexDataProvider(): array
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $element3 = static::createValidElement();

        return [
            [
                'elements' => [$element1, $element2, $element3],
                'indexToRemove' => 0,
                'expected' => [$element2, $element3],
            ],
            [
                'elements' => [$element1, $element2, $element3],
                'indexToRemove' => 1,
                'expected' => [$element1, $element3],
            ],
            [
                'elements' => [$element1, $element2, $element3],
                'indexToRemove' => 2,
                'expected' => [$element1, $element2],
            ],
        ];
    }

    public function testRemoveIndexReturnRemovedElement(): void
    {
        $element = static::createValidElement();
        $this->sequence->add($element);

        $result = $this->sequence->removeIndex(0);
        Assert::assertSame($element, $result);
    }

    public function testRemoveIndexThrowExceptionWhenSpecifiedIndexIsLowerThanZero(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->sequence->removeIndex(-1);
    }

    public function testRemoveIndexThrowExceptionWhenSpecifiedIndexIsGreaterThanMaxIndexInThisSequence(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->sequence->removeIndex(2);
    }

    public function testSetReplaceElementAtSpecifiedIndex(): void
    {
        $newElement = static::createValidElement();
        $oldElement = static::createValidElement();

        $this->sequence->add($oldElement);
        $this->sequence->set(0, $newElement);

        $expectedResult = [$newElement];
        Assert::assertEquals($expectedResult, $this->sequence->toArray());
    }

    public function testSetReturnReplacedElement(): void
    {
        $newElement = static::createValidElement();
        $oldElement = static::createValidElement();

        $this->sequence->add($oldElement);
        $result = $this->sequence->set(0, $newElement);

        Assert::assertSame($oldElement, $result);
    }

    public function testSetThrowExceptionWhenSpecifiedIndexIsLowerThanZero(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->sequence->set(-1, $this->createValidElement());
    }

    public function testSetThrowExceptionWhenSpecifiedIndexIsGreaterThanMaxIndexInThisSequence(): void
    {
        $this->expectException(IndexOutOfBoundsException::class);

        $this->sequence->set(2, $this->createValidElement());
    }

    public function testContainsReturnTrueIfSpecifiedElementExistsInCollection(): void
    {
        $element = static::createValidElement();
        $this->sequence->add($element);

        $result = $this->sequence->contains($element);

        Assert::assertTrue($result);
    }

    public function testContainsReturnFalseIfSpecifiedElementNotExistsInCollection(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $this->sequence->add($element1);

        $result = $this->sequence->contains($element2);

        Assert::assertFalse($result);
    }

    public function testContainsAllReturnTrueIfAllSpecifiedElementsAreExistsInCollection(): void
    {
        $elements = [
            $this->createValidElement(),
            $this->createValidElement(),
            $this->createValidElement(),
        ];
        $this->sequence->addAll($elements);

        $result = $this->sequence->containsAll($elements);

        Assert::assertTrue($result);
    }

    public function testContainsAllReturnFalseIfAnyOfSpecifiedElementIsNotExistsInCollection(): void
    {
        $elem1 = static::createValidElement();
        $elem2 = static::createValidElement();
        $elements = [
            $elem1,
            $elem2,
        ];
        $this->sequence->addAll($elements);

        $testedElements = [
            $elem1,
            $elem2,
            $this->createValidElement(),
        ];
        $result = $this->sequence->containsAll($testedElements);

        Assert::assertFalse($result, 'containsAll method return unexpected result');
    }

    public function testIndexOfReturnIndexOfFirstOccurrenceSpecifiedElement(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();

        $elements = [
            $element1,
            $element2,
            $element1,
        ];
        $this->sequence->addAll($elements);

        $result = $this->sequence->indexOf($element1);

        Assert::assertSame(0, $result);
    }

    public function testIndexOfReturnMinusOneIfListDoesNotContainsSpecifiedElement(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $element3 = static::createValidElement();

        $elements = [
            $element1,
            $element2,
        ];
        $this->sequence->addAll($elements);

        $result = $this->sequence->indexOf($element3);

        Assert::assertSame(-1, $result);
    }

    public function testLastIndexOfReturnIndexOfLastOccurrenceSpecifiedElement(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();

        $elements = [
            $element1,
            $element2,
            $element1,
        ];
        $this->sequence->addAll($elements);

        $result = $this->sequence->lastIndexOf($element1);

        Assert::assertSame(2, $result);
    }

    public function testLastIndexOfReturnMinusOneIfListDoesNotContainsSpecifiedElement(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $element3 = static::createValidElement();

        $elements = [
            $element1,
            $element2,
        ];
        $this->sequence->addAll($elements);

        $result = $this->sequence->lastIndexOf($element3);

        Assert::assertSame(-1, $result);
    }

    public function testRemoveRemovesFirstOccurrenceSpecifiedElement(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $element3 = static::createValidElement();
        $elements = [
            $element1,
            $element2,
            $element3,
            $element2,
        ];
        $this->sequence->addAll($elements);

        $this->sequence->remove($element2);

        $expectedElements = [
            $element1,
            $element3,
            $element2,
        ];

        Assert::assertEquals($expectedElements, $this->sequence->toArray());
    }

    public function testRemoveReturnTrueIfCollectionChangeAfterThisCall(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $element3 = static::createValidElement();
        $elements = [
            $element1,
            $element2,
            $element3,
        ];
        $this->sequence->addAll($elements);

        $result = $this->sequence->remove($element2);

        Assert::assertTrue($result, 'remove method return unexpected result');
    }

    public function testRemoveReturnFalseIfCollectionDoesNotChangeAfterThisCall(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $element3 = static::createValidElement();
        $elements = [
            $element1,
            $element3,
        ];
        $this->sequence->addAll($elements);

        $result = $this->sequence->remove($element2);

        Assert::assertFalse($result);
    }

    public function testRemoveAllRemovesAllOccurrencesOfSpecifiedElements(): void
    {
        // given
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $element3 = static::createValidElement();
        $element4 = static::createValidElement();
        $elements = [
            $element1,
            $element2,
            $element3,
            $element1,
            $element4
        ];
        $this->sequence->addAll($elements);

        // when
        $this->sequence->removeAll([$element1, $element4]);

        // then
        $expectedElements = [
            $element2,
            $element3,
        ];

        Assert::assertEquals($expectedElements, $this->sequence->toArray());
    }

    public function testRemoveAllReturnTrueIfCollectionChangeAfterThisCall(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $element3 = static::createValidElement();
        $elements = [
            $element1,
            $element2,
            $element3,
        ];
        $this->sequence->addAll($elements);

        $result = $this->sequence->removeAll([$element2]);

        Assert::assertTrue($result);
    }

    public function testRemoveAllReturnFalseIfCollectionDoesNotChangeAfterThisCall(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $element3 = static::createValidElement();
        $elements = [
            $element1,
            $element3,
        ];
        $this->sequence->addAll($elements);

        $result = $this->sequence->removeAll([$element2]);

        Assert::assertFalse($result);
    }

    public function testRetainAllRemovesAllOccurrencesOfElementsThatAreNotPresentInSpecifiedCollection(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $element3 = static::createValidElement();
        $elements = [
            $element1,
            $element2,
            $element3,
            $element2,
        ];
        $this->sequence->addAll($elements);

        $collection = static::createEmptySequence();
        $collection->addAll([$element2]);
        $this->sequence->retainAll($collection);

        $expectedElements = [
            $element2,
            $element2,
        ];

        Assert::assertEquals($expectedElements, $this->sequence->toArray());
    }

    public function testRetainAllReturnTrueIfCollectionChangeAfterThisCall(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $element3 = static::createValidElement();
        $elements = [
            $element1,
            $element2,
            $element3,
        ];
        $this->sequence->addAll($elements);

        $collection = static::createEmptySequence();
        $collection->addAll([$element2]);
        $result = $this->sequence->retainAll($collection);


        Assert::assertTrue($result);
    }

    public function testRetainAllReturnFalseIfCollectionDoesNotChangeAfterThisCall(): void
    {
        $element1 = static::createValidElement();
        $element2 = static::createValidElement();
        $elements = [
            $element1,
            $element2,
        ];
        $this->sequence->addAll($elements);

        $collection = static::createEmptySequence();
        $collection->addAll([$element1, $element2]);
        $result = $this->sequence->retainAll($collection);

        Assert::assertFalse($result);
    }

    public function testSortUseComparatorToSortElements(): void
    {
        $elem1 = static::createValidElement();
        $elem2 = static::createValidElement();
        $elem3 = static::createValidElement();
        $elem4 = static::createValidElement();
        $this->sequence->addAll([$elem2, $elem3, $elem1, $elem4]);

        $this->sequence->sort($this->createComparator());

        $expectedElements = [$elem1, $elem2, $elem3, $elem4];
        Assert::assertEquals($expectedElements, $this->sequence->toArray());
    }

    public function testSubSequenceReturnSubSequenceWithSpecifiedIndexRange(): void
    {
        $elem1 = static::createValidElement();
        $elem2 = static::createValidElement();
        $elem3 = static::createValidElement();
        $elem4 = static::createValidElement();
        $this->sequence->addAll([$elem1, $elem2, $elem3, $elem4]);

        $subSequence = $this->sequence->subSequence(1, 3);

        Assert::assertInstanceOf(SubSequence::class, $subSequence);

        $expectedElements = [
            $elem2,
            $elem3,
        ];
        Assert::assertEquals($expectedElements, $subSequence->toArray());
    }

    /**
     * @return Comparator<TValue>
     */
    abstract protected function createComparator(): Comparator;
}
