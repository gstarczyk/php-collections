<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\CallbackComparator;
use Gstarczyk\PhpCollections\FloatsSet;
use Gstarczyk\PhpCollections\Set;
use PHPUnit\Framework\Attributes\CoversClass;
use stdClass;

#[CoversClass(FloatsSet::class)]
class FloatsSetWithComparatorTest extends AbstractSetTestCase
{
    /**
     * @return Set<float>
     */
    protected static function createEmptySet(): Set
    {
        $comparator = new CallbackComparator(
            function ($element1, $element2) {
                $result = $element1 - $element2;
                if ($result > 0) {
                    return 1;
                } elseif ($result < 0) {
                    return -1;
                }

                return 0;
            }
        );

        return new FloatsSet($comparator);
    }

    protected static function createValidElement(float $id): float
    {
        return $id * 1.0;
    }

    /**
     * @return array<int, array<int, mixed>>
     */
    public static function invalidElementsProvider(): array
    {
        return [
            [10],
            ['text'],
            [[]],
            [new stdClass()],
        ];
    }
}
