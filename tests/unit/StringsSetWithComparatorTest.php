<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\CallbackComparator;
use Gstarczyk\PhpCollections\Set;
use Gstarczyk\PhpCollections\StringsSet;
use PHPUnit\Framework\Attributes\CoversClass;
use stdClass;

#[CoversClass(StringsSet::class)]
class StringsSetWithComparatorTest extends AbstractSetTestCase
{
    /**
     * @return Set<string>
     */
    protected static function createEmptySet(): Set
    {
        $comparator = new CallbackComparator(
            function ($element1, $element2) {
                return strcmp($element1, $element2);
            }
        );

        return new StringsSet($comparator);
    }

    protected static function createValidElement(float $id): string
    {
        return 'myElement#' . $id;
    }

    /**
     * @return array<array<int, mixed>>
     */
    public static function invalidElementsProvider(): array
    {
        return [
            [1],
            [[]],
            [2.5],
            [new stdClass()],
        ];
    }
}
