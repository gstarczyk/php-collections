<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\CallbackComparator;
use Gstarczyk\PhpCollections\Comparator;
use Gstarczyk\PhpCollections\IntegersSequence;
use Gstarczyk\PhpCollections\Sequence;
use PHPUnit\Framework\Attributes\CoversClass;

/**
 * @extends AbstractSequenceTestCase<int>
 */
#[CoversClass(IntegersSequence::class)]
class IntegersSequenceTest extends AbstractSequenceTestCase
{
    private static int $element = 0;

    protected function setUp(): void
    {
        self::$element = 0;
        parent::setUp();
    }

    protected static function createValidElement(): int
    {
        return self::$element++;
    }

    protected static function createInvalidElement(): string
    {
        return 'non integer element';
    }

    /**
     * @return Sequence<int>
     */
    protected static function createEmptySequence(): Sequence
    {
        return new IntegersSequence();
    }

    /**
     * @return Comparator<int>
     */
    protected function createComparator(): Comparator
    {
        return new CallbackComparator(
            function ($item1, $item2) {
                return $item1 - $item2;
            }
        );
    }
}
