<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\CallbackComparator;
use Gstarczyk\PhpCollections\Comparator;
use Gstarczyk\PhpCollections\Sequence;
use Gstarczyk\PhpCollections\StringsSequence;
use PHPUnit\Framework\Attributes\CoversClass;

/**
 * @extends AbstractSequenceTestCase<string>
 */
#[CoversClass(StringsSequence::class)]
class StringsSequenceTest extends AbstractSequenceTestCase
{
    private static int $elementId = 0;

    protected function setUp(): void
    {
        parent::setUp();
        self::$elementId = 0;
    }

    protected static function createValidElement(): string
    {
        return 'element#' . self::$elementId++;
    }

    protected static function createInvalidElement(): int
    {
        return 1000;
    }

    /**
     * @return Sequence<string>
     */
    protected static function createEmptySequence(): Sequence
    {
        return new StringsSequence();
    }

    /**
     * @return Comparator<string>
     */
    protected function createComparator(): Comparator
    {
        return new CallbackComparator(
            function ($item1, $item2) {
                return strcmp($item1, $item2);
            }
        );
    }
}
