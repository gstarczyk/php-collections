<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NoSuchElementException;
use Gstarczyk\PhpCollections\SortedSet;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\DataProvider;

abstract class AbstractSortedSetTestCase extends AbstractSetTestCase
{
    /**
     * @var SortedSet<mixed>
     */
    protected $testedSet;

    public function testAllAddedElementsAreSorted(): void
    {
        $elem1 = $this->createValidElement(1.1);
        $elem2 = $this->createValidElement(1.2);
        $elem3 = $this->createValidElement(1.3);
        $this->testedSet->addAll([$elem2, $elem3, $elem1]);

        $result = $this->testedSet->toArray();

        $expected = [$elem1, $elem2, $elem3];
        Assert::assertSame($expected, $result);
    }

    public function testFirstReturnsFirstElementCurrentlyInSet(): void
    {
        $elem1 = $this->createValidElement(1.1);
        $elem2 = $this->createValidElement(1.2);
        $elem3 = $this->createValidElement(1.3);
        $this->testedSet->addAll([$elem1, $elem2, $elem3]);

        $first = $this->testedSet->first();

        Assert::assertSame($elem1, $first);
    }

    public function testFirstThrowExceptionWhenSetIsEmpty(): void
    {
        $this->expectException(NoSuchElementException::class);

        $this->testedSet->clear();
        $this->testedSet->first();
    }

    public function testLastReturnsLastElementCurrentlyInSet(): void
    {
        $elem1 = $this->createValidElement(1.1);
        $elem2 = $this->createValidElement(1.2);
        $elem3 = $this->createValidElement(1.3);
        $this->testedSet->addAll([$elem1, $elem2, $elem3]);

        $last = $this->testedSet->last();

        Assert::assertSame($elem3, $last);
    }

    public function testLastThrowExceptionWhenSetIsEmpty(): void
    {
        $this->expectException(NoSuchElementException::class);

        $this->testedSet->clear();
        $this->testedSet->last();
    }

    public function testTailSetReturnSubSetWithAllElementsGreaterOrEqualThanGivenElement(): void
    {
        $elem1 = $this->createValidElement(1.1);
        $elem2 = $this->createValidElement(1.2);
        $elem3 = $this->createValidElement(1.3);
        $elem4 = $this->createValidElement(1.4);
        $elem5 = $this->createValidElement(1.5);
        $this->testedSet->addAll([$elem1, $elem2, $elem3, $elem4, $elem5]);

        $subSet = $this->testedSet->tailSet($elem4);

        $expected = [$elem4, $elem5];
        Assert::assertSame($expected, $subSet->toArray());
    }

    #[DataProvider('invalidElementsProvider')]
    public function testTailSetThrowExceptionWhenGivenFromElementIsInvalid(mixed $fromElement): void
    {
        $this->expectException(InvalidElementException::class);

        $elem1 = $this->createValidElement(1.1);
        $elem2 = $this->createValidElement(1.2);
        $elem3 = $this->createValidElement(1.3);
        $elem4 = $this->createValidElement(1.4);
        $elem5 = $this->createValidElement(1.5);
        $this->testedSet->addAll([$elem1, $elem2, $elem3, $elem4, $elem5]);

        $this->testedSet->tailSet($fromElement);
    }

    public function testHeadSetReturnSubSetWithAllElementsLessThanGivenElement(): void
    {
        $elem1 = $this->createValidElement(1.1);
        $elem2 = $this->createValidElement(1.2);
        $elem3 = $this->createValidElement(1.3);
        $elem4 = $this->createValidElement(1.4);
        $elem5 = $this->createValidElement(1.5);
        $this->testedSet->addAll([$elem1, $elem2, $elem3, $elem4, $elem5]);

        $subSet = $this->testedSet->headSet($elem4);

        $expected = [$elem1, $elem2, $elem3];
        Assert::assertSame($expected, $subSet->toArray());
    }

    #[DataProvider('invalidElementsProvider')]
    public function testHeadSetThrowExceptionWhenGivenToElementIsInvalid(mixed $toElement): void
    {
        $this->expectException(InvalidElementException::class);

        $elem1 = $this->createValidElement(1.1);
        $elem2 = $this->createValidElement(1.2);
        $elem3 = $this->createValidElement(1.3);
        $elem4 = $this->createValidElement(1.4);
        $elem5 = $this->createValidElement(1.5);
        $this->testedSet->addAll([$elem1, $elem2, $elem3, $elem4, $elem5]);

        $this->testedSet->headSet($toElement);
    }

    public function testSubSetReturnSubSetWithAllElementsGreaterOrEqualThanFromElementAndLessThanToElement(): void
    {
        $elem1 = $this->createValidElement(1.1);
        $elem2 = $this->createValidElement(1.2);
        $elem3 = $this->createValidElement(1.3);
        $elem4 = $this->createValidElement(1.4);
        $elem5 = $this->createValidElement(1.5);
        $this->testedSet->addAll([$elem1, $elem2, $elem3, $elem4, $elem5]);

        $subSet = $this->testedSet->subSet($elem2, $elem4);

        $expected = [$elem2, $elem3];
        Assert::assertSame($expected, $subSet->toArray());
    }

    #[DataProvider('invalidSubSetBoundariesProvider')]
    public function testSubSetThrowExceptionWhenAnyElementFromGivenBoundariesIsInvalid(
        mixed $fromElement,
        mixed $toElement
    ): void {
        $this->expectException(InvalidElementException::class);

        $elem1 = $this->createValidElement(1.1);
        $elem2 = $this->createValidElement(1.2);
        $elem3 = $this->createValidElement(1.3);
        $elem4 = $this->createValidElement(1.4);
        $elem5 = $this->createValidElement(1.5);
        $this->testedSet->addAll([$elem1, $elem2, $elem3, $elem4, $elem5]);

        $this->testedSet->subSet($fromElement, $toElement);
    }

    /**
     * @return array<string, array<int, mixed>>
     */
    abstract public static function invalidSubSetBoundariesProvider(): array;
}
