<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\Set;
use Gstarczyk\PhpCollections\StringsSortedSet;
use PHPUnit\Framework\Attributes\CoversClass;
use stdClass;

#[CoversClass(StringsSortedSet::class)]
class StringsSortedSetTest extends AbstractSortedSetTestCase
{
    /**
     * @return Set<string>
     */
    protected static function createEmptySet(): Set
    {
        return new StringsSortedSet();
    }

    protected static function createValidElement(float $id): string
    {
        return 'myElement#' . $id;
    }

    /**
     * @return array<string, array<int, mixed>>
     */
    public static function invalidElementsProvider(): array
    {
        return [
            'integer' => [1],
            'array' => [[]],
            'float' => [2.5],
            'object' => [new stdClass()],
        ];
    }

    /**
     * @return array<string, array<int, mixed>>
     */
    public static function invalidSubSetBoundariesProvider(): array
    {
        return [
            'invalid from' => [1, 'elem'],
            'invalid to' => ['elem', 10],
            'invalid both' => [1, 10],
        ];
    }
}
