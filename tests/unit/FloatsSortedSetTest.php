<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\FloatsSortedSet;
use Gstarczyk\PhpCollections\Set;
use Gstarczyk\PhpCollections\SortedSet;
use PHPUnit\Framework\Attributes\CoversClass;
use stdClass;

#[CoversClass(FloatsSortedSet::class)]
class FloatsSortedSetTest extends AbstractSortedSetTestCase
{
    /**
     * @return SortedSet<float>
     */
    protected static function createEmptySet(): Set
    {
        return new FloatsSortedSet();
    }

    protected static function createValidElement(float $id): float
    {
        return $id * 1.0;
    }

    /**
     *
     * @return array<string, array<int, mixed>>
     */
    public static function invalidElementsProvider(): array
    {
        return [
            'string' => ['asd'],
            'array' => [[]],
            'integer' => [2],
            'object' => [new stdClass()],
        ];
    }

    /**
     * @return array<string, array<int, mixed>>
     */
    public static function invalidSubSetBoundariesProvider(): array
    {
        return [
            'invalid from' => ['elem', 3.1],
            'invalid to' => [1.1, 'elem'],
            'invalid both' => ['elem1', 'elem2'],
        ];
    }
}
