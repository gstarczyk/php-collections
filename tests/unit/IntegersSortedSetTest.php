<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\IntegersSortedSet;
use Gstarczyk\PhpCollections\Set;
use Gstarczyk\PhpCollections\SortedSet;
use PHPUnit\Framework\Attributes\CoversClass;
use stdClass;

#[CoversClass(IntegersSortedSet::class)]
class IntegersSortedSetTest extends AbstractSortedSetTestCase
{
    /**
     * @return SortedSet<int>
     */
    protected static function createEmptySet(): Set
    {
        return new IntegersSortedSet();
    }

    protected static function createValidElement(float $id): mixed
    {
        return (int)($id * 10);
    }

    /**
     * @return array<string, array<int, mixed>>
     */
    public static function invalidElementsProvider(): array
    {
        return [
            'string' => ['asd'],
            'array' => [[]],
            'float' => [2.5],
            'object' => [new stdClass()],
        ];
    }

    /**
     * @return array<string, array<int, mixed>>
     */
    public static function invalidSubSetBoundariesProvider(): array
    {
        return [
            'invalid from' => ['elem', 3],
            'invalid to' => [1, 'elem'],
            'invalid both' => ['elem1', 'elem2'],
        ];
    }
}
