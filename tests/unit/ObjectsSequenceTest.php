<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\CallbackComparator;
use Gstarczyk\PhpCollections\Comparator;
use Gstarczyk\PhpCollections\ObjectsSequence;
use Gstarczyk\PhpCollections\Sequence;
use Gstarczyk\PhpCollections\TestFixture\CollectionElement;
use PHPUnit\Framework\Attributes\CoversClass;

/**
 * @extends AbstractSequenceTestCase<CollectionElement>
 */
#[CoversClass(ObjectsSequence::class)]
class ObjectsSequenceTest extends AbstractSequenceTestCase
{
    private static int $elementId = 0;

    protected function setUp(): void
    {
        $this->sequence = $this->createEmptySequence();
        self::$elementId = 0;
    }

    protected static function createValidElement(): CollectionElement
    {
        return new CollectionElement(sprintf('element #%u', self::$elementId++));
    }

    protected static function createInvalidElement(): string
    {
        return '';
    }

    /**
     * @return Sequence<CollectionElement>
     */
    protected static function createEmptySequence(): Sequence
    {
        return new ObjectsSequence(CollectionElement::class);
    }

    /**
     * @return Comparator<CollectionElement>
     */
    protected function createComparator(): Comparator
    {
        return new CallbackComparator(
            function (CollectionElement $item1, CollectionElement $item2) {
                $name1 = $item1->name;
                $name2 = $item2->name;
                if ($name1 > $name2) {
                    return 1;
                } elseif ($name1 == $name2) {
                    return 0;
                } else {
                    return -1;
                }
            }
        );
    }
}
