<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Set;
use Gstarczyk\PhpCollections\SortedSet;
use Gstarczyk\PhpCollections\SortedSubSet;
use Gstarczyk\PhpCollections\StringsSortedSet;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;

#[CoversClass(SortedSubSet::class)]
class SortedSubSetTest extends AbstractSortedSetTestCase
{
    /**
     * @var SortedSet<string>
     */
    private SortedSet $parentSet;

    protected function setUp(): void
    {
        $this->parentSet = new StringsSortedSet();
        $this->parentSet->addAll(
            [
                'elem1',
                'elem2',
                'elem3',
                'elem4',
                'elem5',
                'elem6',
            ]
        );

        parent::setUp();
    }

    /**
     * @return Set<string>
     */
    protected static function createEmptySet(): Set
    {
        $parentSet = new StringsSortedSet();
        $fromElement = static::createValidElement(1.0);
        $toElement = static::createValidElement(100.0);

        return new SortedSubSet($parentSet, $fromElement, $toElement);
    }

    protected static function createValidElement(float $id): string
    {
        return sprintf('elem#%05d', (int)($id * 1000));
    }

    /**
     * @return array<int, array<int, mixed>>
     */
    public static function invalidElementsProvider(): array
    {
        return [
            ['el0'],
        ];
    }

    /**
     * @param array<string> $expected
     */
    #[DataProvider(methodName: 'subSetConstraintsProvider')]
    public function testSubSetContainsOnlyThoseElementsFromParentSetThanMatchConstraints(
        string $from,
        string $to,
        array $expected
    ): void {
        $testedSubSet = new SortedSubSet($this->parentSet, $from, $to);

        $result = $testedSubSet->toArray();

        Assert::assertSame($expected, $result);
    }

    /**
     * @return array<string, array<string, mixed>>
     */
    public static function subSetConstraintsProvider(): array
    {
        return [
            'strict boundaries' => [
                'from' => 'elem2',
                'to' => 'elem5',
                'expected' => ['elem2', 'elem3', 'elem4'],
            ],
            'loose boundaries' => [
                'from' => 'elem1.1',
                'to' => 'elem4.5',
                'expected' => ['elem2', 'elem3', 'elem4'],
            ],
        ];
    }

    public function testAddingElementAddsAlsoToParentSet(): void
    {
        $testedSubSet = new SortedSubSet($this->parentSet, 'elem2', 'elem5');

        $testedSubSet->add('elem2.5');
        $result = $this->parentSet->contains('elem2.5');

        Assert::assertTrue($result);
    }

    #[DataProvider('outOfBoundariesElementProvider')]
    public function testAddingElementThrowsExceptionWhenGivenElemDoNotMetSubSetBoundaries(string $element): void
    {
        $this->expectException(InvalidElementException::class);

        $testedSubSet = new SortedSubSet($this->parentSet, 'elem2', 'elem5');

        $testedSubSet->add($element);
    }

    /**
     * @return array<string, array<int, mixed>>
     */
    public static function outOfBoundariesElementProvider(): array
    {
        return [
            'too low' => ['elem1'],
            'too high' => ['elem6'],
        ];
    }

    public function testCountReturnNumberOfSubsetElements(): void
    {
        $testedSubSet = new SortedSubSet($this->parentSet, 'elem2', 'elem5');

        $count = $testedSubSet->count();

        Assert::assertEquals(3, $count);
    }

    /**
     * @return array<string, array<int, mixed>>
     */
    public static function invalidSubSetBoundariesProvider(): array
    {
        return [
            'to small fromElement' => [
                static::createValidElement(0.0),
                static::createValidElement(0.7),
            ],
            'to high toElement' => [
                static::createValidElement(0.1),
                static::createValidElement(0.900),
            ],
            'invalid both' => [
                static::createValidElement(0.0),
                static::createValidElement(0.900),
            ],
        ];
    }
}
