<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\FloatsSet;
use Gstarczyk\PhpCollections\Set;
use PHPUnit\Framework\Attributes\CoversClass;
use stdClass;

#[CoversClass(FloatsSet::class)]
class FloatsSetTest extends AbstractSetTestCase
{
    /**
     * @return Set<float>
     */
    protected static function createEmptySet(): Set
    {
        return new FloatsSet();
    }

    protected static function createValidElement(float $id): float
    {
        return $id * 1.0;
    }

    /**
     * @return array<int, array<int, mixed>>
     */
    public static function invalidElementsProvider(): array
    {
        return [
            [10],
            ['text'],
            [[]],
            [new stdClass()],
        ];
    }
}
