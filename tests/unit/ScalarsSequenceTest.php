<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\CallbackComparator;
use Gstarczyk\PhpCollections\Comparator;
use Gstarczyk\PhpCollections\ScalarsSequence;
use Gstarczyk\PhpCollections\Sequence;
use PHPUnit\Framework\Attributes\CoversClass;
use stdClass;

/**
 * @extends AbstractSequenceTestCase<scalar>
 */
#[CoversClass(ScalarsSequence::class)]
class ScalarsSequenceTest extends AbstractSequenceTestCase
{
    private static int $elementId = 0;

    /**
     * @var array<numeric>
     */
    private static array $validElements = [
        1.8,
        2,
        3.1,
        4.3,
        '5',
        6,
        '7',
        8.5,
        9.8,
    ];

    protected function setUp(): void
    {
        parent::setUp();
        self::$elementId = 0;
    }

    protected static function createValidElement(): mixed
    {
        if (array_key_exists(self::$elementId, self::$validElements)) {
            $element = self::$validElements[self::$elementId];
        } else {
            $element = self::$elementId + 1000;
        }
        ++self::$elementId;

        return $element;
    }

    protected static function createInvalidElement(): stdClass
    {
        return new stdClass();
    }

    /**
     * @return Sequence<scalar>
     */
    protected static function createEmptySequence(): Sequence
    {
        return new ScalarsSequence();
    }

    /**
     * @return Comparator<scalar>
     */
    protected function createComparator(): Comparator
    {
        return new CallbackComparator(
            function ($name1, $name2) {
                if ($name1 > $name2) {
                    return 1;
                } elseif ($name1 == $name2) {
                    return 0;
                } else {
                    return -1;
                }
            }
        );
    }
}
