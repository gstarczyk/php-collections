<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\IntegersSet;
use Gstarczyk\PhpCollections\Set;
use PHPUnit\Framework\Attributes\CoversClass;
use stdClass;

#[CoversClass(IntegersSet::class)]
class IntegersSetTest extends AbstractSetTestCase
{
    /**
     * @return Set<int>
     */
    protected static function createEmptySet(): Set
    {
        return new IntegersSet();
    }

    protected static function createValidElement(float $id): int
    {
        return (int)($id * 10);
    }

    /**
     * @return array<int, array<int, mixed>>
     */
    public static function invalidElementsProvider(): array
    {
        return [
            ['text'],
            [5.5],
            [[]],
            [new stdClass()],
        ];
    }
}
