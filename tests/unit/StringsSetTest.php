<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\Set;
use Gstarczyk\PhpCollections\StringsSet;
use PHPUnit\Framework\Attributes\CoversClass;
use stdClass;

#[CoversClass(StringsSet::class)]
class StringsSetTest extends AbstractSetTestCase
{
    /**
     * @return Set<string>
     */
    protected static function createEmptySet(): Set
    {
        return new StringsSet();
    }

    protected static function createValidElement(float $id): string
    {
        return 'myElement#' . $id;
    }

    /**
     * @return array<int,array<int,mixed>>
     */
    public static function invalidElementsProvider(): array
    {
        return [
            [1],
            [[]],
            [2.5],
            [new stdClass()],
        ];
    }
}
