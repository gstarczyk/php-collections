<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\ObjectsSet;
use Gstarczyk\PhpCollections\Set;
use Gstarczyk\PhpCollections\TestFixture\CollectionElement;
use PHPUnit\Framework\Attributes\CoversClass;
use stdClass;

#[CoversClass(ObjectsSet::class)]
class ObjectsSetTest extends AbstractSetTestCase
{
    /**
     * @return Set<CollectionElement>
     */
    protected static function createEmptySet(): Set
    {
        return new ObjectsSet(CollectionElement::class);
    }

    protected static function createValidElement(float $id): mixed
    {
        return new CollectionElement('element#' . $id);
    }

    /**
     * @return array<int, array<int, mixed>>
     */
    public static function invalidElementsProvider(): array
    {
        return [
            [1],
            ['text'],
            [1.5],
            [new stdClass()],
            [[]],
        ];
    }
}
