<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\CallbackComparator;
use Gstarczyk\PhpCollections\ObjectsSet;
use Gstarczyk\PhpCollections\Set;
use Gstarczyk\PhpCollections\TestFixture\CollectionElement;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\CoversClass;
use stdClass;

#[CoversClass(ObjectsSet::class)]
class ObjectsSetWithComparatorTest extends AbstractSetTestCase
{
    /**
     * @return Set<CollectionElement>
     */
    protected static function createEmptySet(): Set
    {
        // compare elements only by "name" attribute, all others attributes are ignored during comparison
        $comparator = new CallbackComparator(
            function (CollectionElement $element1, CollectionElement $element2) {
                return strcmp($element1->name, $element2->name);
            }
        );

        return new ObjectsSet(CollectionElement::class, $comparator);
    }

    protected static function createValidElement(float $id, string $additionalAttribute = ''): CollectionElement
    {
        return new CollectionElement('element#' . $id, $additionalAttribute);
    }

    /**
     * @return array<int, array<int, mixed>>
     */
    public static function invalidElementsProvider(): array
    {
        return [
            [1],
            ['text'],
            [1.5],
            [new stdClass()],
            [[]],
        ];
    }

    public function testCannotAddElementIfItAlreadyPresentInSet(): void
    {
        $result1 = $this->testedSet->add($this->createValidElement(1.0, 'desc1'));
        $result2 = $this->testedSet->add($this->createValidElement(1.0, 'desc2'));

        Assert::assertTrue($result1);
        Assert::assertFalse($result2);
    }

    public function testRemoveReturnTrueIfSetIsChanged(): void
    {
        $element1 = $this->createValidElement(1.1, 'desc1');
        $element2 = $this->createValidElement(1.2);
        $this->testedSet->addAll([$element1, $element2]);

        $result = $this->testedSet->remove($this->createValidElement(1.1, 'desc2'));

        Assert::assertTrue($result);
        Assert::assertEquals([$element2], $this->testedSet->toArray());
    }
}
