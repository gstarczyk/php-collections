<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\UnitTest;

use Gstarczyk\PhpCollections\Exceptions\InvalidElementException;
use Gstarczyk\PhpCollections\Exceptions\NullElementException;
use Gstarczyk\PhpCollections\Set;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

abstract class AbstractSetTestCase extends TestCase
{
    /**
     * @var Set<mixed>
     */
    protected $testedSet;

    protected function setUp(): void
    {
        $this->testedSet = $this->createEmptySet();
    }

    /**
     * @return Set<mixed>
     */
    abstract protected static function createEmptySet(): Set;

    abstract protected static function createValidElement(float $id): mixed;

    /**
     * @return array<string, array<int, mixed>>
     */
    abstract public static function invalidElementsProvider(): array;

    public function testCannotAddElementIfItAlreadyPresentInSet(): void
    {
        $element = $this->createValidElement(1.0);
        $result1 = $this->testedSet->add($element);
        $result2 = $this->testedSet->add($element);

        Assert::assertTrue($result1);
        Assert::assertFalse($result2);
        Assert::assertEquals([$element], $this->testedSet->toArray());
    }

    #[DataProvider('invalidElementsProvider')]
    public function testAddThrowExceptionWhenInvalidElementWasGiven(mixed $element): void
    {
        $this->expectException(InvalidElementException::class);
        $this->testedSet->add($element);
    }

    public function testAddThrowExceptionWhenNullElementWasGiven(): void
    {
        $this->expectException(NullElementException::class);
        $this->testedSet->add(null);
    }

    public function testRemoveReturnTrueIfSetIsChanged(): void
    {
        $element1 = $this->createValidElement(1.1);
        $element2 = $this->createValidElement(1.2);
        $this->testedSet->addAll([$element1, $element2]);

        $result = $this->testedSet->remove($element1);

        Assert::assertTrue($result);
        Assert::assertEquals([$element2], $this->testedSet->toArray());
    }

    public function testRetainAllRemoveAllElementsTharAreNotPresentInGivenCollection(): void
    {
        $element1 = $this->createValidElement(1.1);
        $element2 = $this->createValidElement(1.2);
        $element3 = $this->createValidElement(1.3);
        $element4 = $this->createValidElement(1.4);
        $this->testedSet->addAll(
            [
                $element1,
                $element2,
                $element3,
                $element4,
            ]
        );
        $retainedElements = [$element3, $element4];

        $result = $this->testedSet->retainAll($retainedElements);

        Assert::assertTrue($result);
        Assert::assertEquals($retainedElements, $this->testedSet->toArray());
    }

    #[DataProvider('invalidElementsProvider')]
    public function testAddAllThrowExceptionWhenGivenElementIsInvalid(mixed $invalidElement): void
    {
        $this->expectException(InvalidElementException::class);

        $this->testedSet->addAll([$this->createValidElement(1.0), $invalidElement]);
    }

    public function testAddAllThrowExceptionWhenGivenElementIsNull(): void
    {
        $this->expectException(NullElementException::class);

        $this->testedSet->addAll([$this->createValidElement(1.0), null]);
    }

    public function testContainsAllReturnFalseWhenOneOrMoreGivenElementsAreNotContainedInThisSet(): void
    {
        $element1 = $this->createValidElement(1.1);
        $element2 = $this->createValidElement(1.2);
        $element3 = $this->createValidElement(1.3);
        $element4 = $this->createValidElement(1.4);
        $this->testedSet->addAll([$element1, $element2, $element3]);

        $result = $this->testedSet->containsAll([$element3, $element4]);

        Assert::assertFalse($result);
    }

    public function testRemoveAllRemovesGivenElementsFromSet(): void
    {
        $element1 = $this->createValidElement(1.1);
        $element2 = $this->createValidElement(1.2);
        $element3 = $this->createValidElement(1.3);
        $this->testedSet->addAll([$element1, $element2, $element3]);

        $this->testedSet->removeAll([$element1, $element2]);

        Assert::assertEquals([$element3], $this->testedSet->toArray());
    }

    public function testClearRemovesAllElementsFromSet(): void
    {
        $element1 = $this->createValidElement(1.1);
        $element2 = $this->createValidElement(1.2);
        $element3 = $this->createValidElement(1.3);
        $this->testedSet->addAll([$element1, $element2, $element3]);

        $this->testedSet->clear();

        Assert::assertEmpty($this->testedSet->toArray());
    }

    /**
     * @param array<int, mixed> $set1Elements
     * @param array<int, mixed> $set2Elements
     */
    #[DataProvider('equalSetsProvider')]
    public function testEqualsReturnTrueWhenTooSetsHaveSameSizeAndElements(
        array $set1Elements,
        array $set2Elements
    ): void {
        $this->testedSet->addAll($set1Elements);

        $otherSet = $this->createEmptySet();
        $otherSet->addAll($set2Elements);

        $result = $this->testedSet->equals($otherSet);

        Assert::assertTrue($result);
    }

    /**
     * @return array<string, array<string, mixed>>
     */
    public static function equalSetsProvider(): array
    {
        $element1 = static::createValidElement(1.1);
        $element2 = static::createValidElement(1.2);
        $element3 = static::createValidElement(1.3);

        return [
            'same order, same instances' => [
                'set1Elements' => [
                    $element1,
                    $element2,
                    $element3,
                ],
                'set2Elements' => [
                    $element1,
                    $element2,
                    $element3,
                ],
            ],
            'different order, same instances' => [
                'set1Elements' => [
                    $element1,
                    $element2,
                    $element3,
                ],
                'set2Elements' => [
                    $element3,
                    $element1,
                    $element2,
                ],
            ],
            'same order, different instances' => [
                'set1Elements' => [
                    static::createValidElement(1.1),
                    static::createValidElement(1.2),
                    static::createValidElement(1.3),
                ],
                'set2Elements' => [
                    static::createValidElement(1.1),
                    static::createValidElement(1.2),
                    static::createValidElement(1.3),
                ],
            ],
            'different order, different instances' => [
                'set1Elements' => [
                    static::createValidElement(1.1),
                    static::createValidElement(1.2),
                    static::createValidElement(1.3),
                ],
                'set2Elements' => [
                    static::createValidElement(1.3),
                    static::createValidElement(1.1),
                    static::createValidElement(1.2),
                ],
            ],
        ];
    }

    /**
     * @param array<int, mixed> $set1Elements
     * @param array<int, mixed> $set2Elements
     */
    #[DataProvider('differentSetsProvider')]
    public function testEqualsReturnFalseWhenGivenSetHasDifferentSizeOrDifferentElements(
        array $set1Elements,
        array $set2Elements
    ): void {
        $this->testedSet->addAll($set1Elements);

        $otherSet = $this->createEmptySet();
        $otherSet->addAll($set2Elements);

        $result = $this->testedSet->equals($otherSet);

        Assert::assertFalse($result);
    }

    /**
     * @return array<string, array<string, mixed>>
     */
    public static function differentSetsProvider(): array
    {
        return [
            'different size' => [
                'set1Elements' => [
                    static::createValidElement(1.1),
                    static::createValidElement(1.2)
                ],
                'set2Elements' => [
                    static::createValidElement(1.1),
                    static::createValidElement(1.2),
                    static::createValidElement(1.3),
                ],
            ],
            'different elements' => [
                'set1Elements' => [
                    static::createValidElement(1.1),
                    static::createValidElement(1.2)
                ],
                'set2Elements' => [
                    static::createValidElement(1.1),
                    static::createValidElement(1.3),
                ],
            ],
        ];
    }
}
