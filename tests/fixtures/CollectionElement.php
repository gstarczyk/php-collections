<?php

declare(strict_types=1);

namespace Gstarczyk\PhpCollections\TestFixture;

class CollectionElement
{
    public string $name;
    public string $additionalAttribute;

    public function __construct(string $name, string $additionalAttribute = '')
    {
        $this->name = $name;
        $this->additionalAttribute = $additionalAttribute;
    }
}
